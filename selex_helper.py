#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Miscellaneous helper scripts

import sys
import argparse

def main(argv=None):
    if argv is None:
        argv = sys.argv
    
    parser = argparse.ArgumentParser(description="Helper scripts for aptamer comparisons")
    parser.add_argument('-i', '--infile', required=True,
                        help="Input file name")
    parser.add_argument('-o', '--outfile', required=False, default='outfile.txt',
                        help="Output file name")
    parser.add_argument('--add_constants', required=False, default='', nargs=2,
                        help="Add constant regions to a FASTA file. Requires "
                        "constant regions as arguments. Example: __add_constants "
                        "constant constant")
    parser.add_argument('--fasta2fastq', required=False, action='store_true',
                        help="Convert FASTA file to FASTQ")
    parser.add_argument('--dna2rna', required=False, action='store_true',
                        help="Convert DNA sequences to RNA sequences")
    args = parser.parse_args()
    infile = args.infile
    outfile = args.outfile
    constants = args.add_constants
    run_fasta2fastq = args.fasta2fastq
    run_dna2rna = args.dna2rna
    
    if constants:
        add_constants(infile, outfile, constants[0], constants[1])
    if run_fasta2fastq:
        fasta2fastq(infile, outfile)
    if run_dna2rna:
        ext = outfile[outfile.rfind('.')+1:]
        dna2rna(infile, outfile, format=ext)
    
def add_constants(infile, outfile, c1, c2):
    """Adds constant regions to a FASTA file."""
    with open(infile, 'r') as ih:
        with open(outfile, 'w') as oh:
            while True:
                line = ih.readline()
                if not line:
                    break
                else:
                    oh.write(line)
                    seq = ih.readline().strip()
                    oh.write(c1 + seq + c2 + "\n")
    return

def fasta2fastq(infile, outfile):
    """Converts a FASTA file to FASTQ."""
    with open(infile, 'r') as ih:
        with open(outfile, 'w') as oh:
            while True:
                line = ih.readline()
                if not line:
                    break
                else:
                    oh.write(line)
                    seq = ih.readline().strip()
                    oh.write(seq + "\n")
                    oh.write("+\n")
                    oh.write("I" * len(seq) + "\n")
    return

def dna2rna(infile, outfile, format='fasta'):
    """Converts a DNA file to RNA"""
    if format == 'fasta':
        with open(infile, 'r') as ih:
            with open(outfile, 'w') as oh:
                while True:
                    line = ih.readline()
                    if not line:
                        break
                    else:
                        oh.write(line)
                        seq = ih.readline().strip()
                        seq.replace('T', 'U')
                        oh.write(seq + "\n")
    elif format in ['seq', 'sequence']:
        with open(infile, 'r') as ih:
            with open(outfile, 'w') as oh:
                while True:
                    line = ih.readline()
                    if not line:
                        break
                    else:
                        seq = line.replace('T', 'U')
                        oh.write(seq)
    return

if __name__ == "__main__":
    sys.exit(main())
