#!/usr/bin/env python

# read_fastaptamer.py
# Reads a FASTAptamer file

import sys
import argparse
import operator

def main(argv=None):
    if argv is None:
        argv = sys.argv
    
    parser = argparse.ArgumentParser(description="Reads a FASTAptamer file")
    parser.add_argument('-i', '--infile', required=True,
                        help="Input file")
    parser.add_argument('-c', '--counter', required=False, type=int,
                        default=1000000, help="Number of sequences to read")
    parser.add_argument('-t', '--type', required=False, default='count',
                        help="File type. Either 'count' or 'cluster'")
    args = parser.parse_args()
    infile = args.infile
    counter = args.counter
    file_type = args.type
    
    d = dict()
    ih = open(infile, 'r')
    while counter > 0:
        line = ih.readline().strip()
        if not line:
            break
        else:
            seqid = line[1:]    
            seq = ih.readline().strip()
            if file_type == 'count':
                rank, reads, rpm = seqid.split('-')
                d[seq] = ((rank, reads, rpm))
            else:
                rank, reads, rpm, cluster, c_rank, dist = seqid.split('-')
                d[seq] = ((rank, reads, rpm, cluster, c_rank, dist))
            counter -= 1
    
    print(d)
    sorted_d = sorted(list(d.items()), key=operator.itemgetter(1))
    print(sorted_d)
    return d

if __name__ == "__main__":
    sys.exit(main())
