#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Python script to convert SELEX reads into formats for various bioinformatics
# programs.
#
# In order to make comparisons, the data must be as uniform as possible. For
# example, some programs require sequences only, 1 per line, while others
# require FASTA or FASTQ files. Some programs allow for mismatches in constant
# regions, and others need these adapters removed.
#
# Therefore, we use 'cutadapt' to trim the constant regions, and then add
# the constant regions back (without errors). The 'cutadapt' program must be
# in the search path (PATH variable).
# 
# Author: Kevin Shieh
# Date: November 25, 2015
#
# Usage: python selex_script.py -i <file.fasta> -o <outfile.fasta>

import sys
import argparse
import subprocess
from os.path import dirname, join

def main(argv=None):
    if argv is None:
        argv = sys.argv
    
    parser = argparse.ArgumentParser(description="Python script to convert \
    SELEX reads into formats for various bioinformatics programs.")
    parser.add_argument('-i', '--infile', required=True,
                        help="Input file name. Required.")
    parser.add_argument('--informat', required=True,
                        help="Input file format. Optional. Must be 'fastq', \
                        'fasta', or 'sequence'.")
    parser.add_argument('-o', '--outfile', required=True,
                        help="Output file prefix. Required.")
    parser.add_argument('--outformat', required=False, nargs = '+',
                        help="List of output file formats. Valid file formats \
                        are: sequence, fasta, fastq")
    parser.add_argument('-a', '--allgen', required=False, action='store_true',
                        help="Generate all output formats.")
    parser.add_argument('-c', '--constants', required=False, nargs=2,
                        help="Constant regions. Must be given if '--cutadapt' \
                        is used. Example: '-c ACGT TGCA'")
    parser.add_argument('--noambig', action='store_true',
                        help="Exclude any sequences with ambiguous (N) nucleotides.")
    parser.add_argument('--cutadapt', required=False, action='store_true',
                        help="Run cutadapt on the sequences")
    parser.add_argument('--cutadapt_flags', required=False, default=["-e 0.2 -m 45 -M 55"], nargs=argparse.REMAINDER,
                        help="Run cutadapt with the constant regions given \
                        above. This must be the last argument, and the file \
                        format and constant regions are added. Arguments given \
                        in quotes will be passed to cutadapt. \
                        Example: --cutadapt \"-e 0.2 -m 45 -M 55\" becomes \
                        cutadapt -f <fastq> -g <5'-const> -a <3'-const> -e 0.2 \
                        -m 45 -M 55")
    args = parser.parse_args()
    infile = args.infile
    informat = args.informat
    outfile_pref = args.outfile
    outformat = args.outformat
    all_flag = args.allgen
    constants = args.constants
    no_ambig = args.noambig
    cutadapt = args.cutadapt
    cutadapt_flags = args.cutadapt_flags
    
    # Check arguments
    if cutadapt:
        if not constants:
            raise IOError("Running cutadapt requires constants to be specified.")
    if not outformat:
        if not all_flag:
            raise IOError("Either specify output formats, or set 'allgen' to \
                          generate all output formats.")
    
    # Read from input file. Trim adapters if cutadapt specified.
    temp_output = 'temp.out'
    if cutadapt:
        # Run cutadapt and keep list of trimmed and corrected sequence lists
        stdout, stderr = run_cutadapt(infile, informat, constants[0],
                                      constants[1], ' '.join(cutadapt_flags), temp_output)
        trimmed_list = read_fastq_file(temp_output)
        seq_list = add_constants(trimmed_list, constants[0], constants[1])
    else:
        # Determine input file format and read input
        if informat == 'sequence':
            seq_list = read_seq_file(infile)
        elif informat == 'fasta':
            seq_list = read_fasta_file(infile)
        elif informat == 'fastq':
            seq_list = read_fastq_file(infile)
        else:
            raise IOError("Unknown input file format: {0}".format(informat))
    
    # Remove sequences containing ambiguous nucleotides
    if cutadapt:
        trimmed_list = remove_ambig(trimmed_list)
    seq_list = remove_ambig(seq_list)
    
    # Generate output files
    out_dir = get_dirname(infile)
    if all_flag:
        if cutadapt:
            write_seq_file(join(out_dir, outfile_pref + '_trim'), trimmed_list)
            write_fasta_file(join(out_dir, outfile_pref + '_trim'), trimmed_list)
            write_fastq_file(join(out_dir, outfile_pref + '_trim'), trimmed_list)
        write_seq_file(join(out_dir, outfile_pref), seq_list)
        write_fasta_file(join(out_dir, outfile_pref), seq_list)
        write_fastq_file(join(out_dir, outfile_pref), seq_list)
    else:
        for i in outformat:
            if i == 'sequence':
                if cutadapt:
                    write_seq_file(join(out_dir, outfile_pref + '_trim'), trimmed_list)
                write_seq_file(join(out_dir, outfile_pref), seq_list)
            elif i == 'fasta':
                if cutadapt:
                    write_fasta_file(join(out_dir, outfile_pref + '_trim'), trimmed_list)
                write_fasta_file(join(out_dir, outfile_pref), seq_list)
            elif i == 'fastq':
                if cutadapt:
                    write_fastq_file(join(out_dir, outfile_pref + '_trim'), trimmed_list)
                write_fastq_file(join(out_dir, outfile_pref), seq_list)
            else:
                print("Unknown output format: {0}".format(i))
    
    # Clean files
    if cutadapt:
        subprocess.call("rm {0}".format(temp_output), shell=True)
    

def get_format(s):
    """Returns a file format from a file name containing an extension."""
    if '.' in s:
        idx = s.rfind('.')
        ext = s[idx+1:]
        if ext in ['sequence', 'seq']:
            return 'sequence'
        elif ext in ['fasta', 'fa']:
            return 'fasta'
        elif ext in ['fastq', 'fq']:
            return 'fastq'
        else:
            return None
    else:
        return None

def get_dirname(path):
    return dirname(path)
    
def read_file(file_name):
    """Opens a file handle to read from a file."""
    try:
        fh = open(file_name, 'r')
    except IOError:
        raise IOError("Unable to open input file!")
    else:
        return fh

def read_seq_file(file_name):
    """Reads a sequence file (i.e., 1 sequence per line). Gives each sequence
    a unique name ('Sequence#')."""
    seq_list = list()
    fh = read_file(file_name)
    ctr = 1
    while True:
        line = fh.readline().strip()
        if not line:
            break
        else:
            seq_list.append(("Sequence{0}".format(ctr), line)) # Append (seqid, seq) tuple
            ctr += 1
    fh.close()
    return seq_list

def read_fasta_file(file_name):
    """Reads a FASTA file."""
    seq_list = list()
    fh = read_file(file_name)
    seq = ''
    while True:
        line = fh.readline().strip()
        if not line:
            if seq:
                seq_list.append((seqid, seq))
            break
        else:
            if line[0] == '>':
                if seq:
                    seq_list.append((seqid, seq))
                seqid = line[1:]
                buf = fh.readline().strip()
                seq = buf
            else:
                seq += line
    fh.close()
    return seq_list

def read_fastq_file(file_name):
    """Reads a FASTQ file."""
    seq_list = list()
    fh = read_file(file_name)
    while True:
        line = fh.readline().strip()
        if not line:
            break
        else:
            if line[0] == '@':
                seqid = line[1:]
                seq = fh.readline().strip()
            elif line[0] == '+':
                seq_list.append((seqid, seq))
                line = fh.readline()
            else:
                seq += line
    fh.close()
    return seq_list

def write_file(file_name):
    """Opens a file handle to write to a file."""
    try:
        fh = open(file_name, 'w')
    except IOError:
        raise IOError("Unable to open output file!")
    else:
        return fh

def write_seq_file(file_pref, seq_list):
    """Writes a sequence file (i.e., 1 sequence per line)."""
    file_name = file_pref + '.seq'
    fh = write_file(file_name)
    for seqid, seq in seq_list:
        fh.write(seq + "\n")
    fh.close()
    print("Created sequence file: {0}".format(file_name))

def write_fasta_file(file_pref, seq_list):
    """Writes a FASTA file."""
    file_name = file_pref + '.fasta'
    fh = write_file(file_name)
    for seqid, seq in seq_list:
        fh.write(">" + seqid + "\n")
        fh.write(seq + "\n")
    fh.close()
    print("Created FASTA file: {0}".format(file_name))

def write_fastq_file(file_pref, seq_list):
    """Writes a FASTQ file. For the quality values, we assume the Illumina 1.8
    pipeline and encode I (i.e., Phread score of 40) for each nucleotide."""
    file_name = file_pref + '.fastq'
    fh = write_file(file_name)
    for seqid, seq in seq_list:
        fh.write("@" + seqid + "\n")
        fh.write(seq + "\n")
        fh.write("+\n")
        fh.write("I"*len(seq) + "\n")
    fh.close()
    print("Created FASTQ file: {0}".format(file_name))
    
def run_cutadapt(infile, informat, c1, c2, cutadapt_args, outfile):
    """Runs cutadapt on a FASTA file. The cutadapt binary must be in your
    search path (PATH)."""
    if informat == 'fastq':
        print("Running cutadapt in two parts:")
        p = subprocess.Popen("cutadapt -g {0} -e 0.2 --discard-untrimmed -o tempfile.fastq {1}".format(c1, infile), shell=True)
        out, err = p.communicate()
        p = subprocess.Popen("cutadapt -a {0} {1} --discard-untrimmed -o {2} tempfile.fastq".format(c2, cutadapt_args, outfile), shell=True)
        out, err = p.communicate()
        p = subprocess.call("rm -f tempfile.fastq", shell=True)
        return out, err

def add_constants(seq_list, c1, c2):
    """Adds constant regions to sequences."""
    new_seq_list = list()
    for seqid, seq in seq_list:
        new_seq = c1 + seq + c2
        new_seq_list.append((seqid, new_seq))
    return new_seq_list

def remove_ambig(seq_list):
    """Removes ambiguous sequences from sequence list."""
    new_seq_list = list()
    for seqid, seq in seq_list:
        if 'N' not in seq.upper():
            new_seq_list.append((seqid, seq))
    return new_seq_list


if __name__ == "__main__":
    sys.exit(main())
