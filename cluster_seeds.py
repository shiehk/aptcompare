#!/usr/bin/env python

# Run fastaptamer_cluster to determine cluster seeds

import sys
import argparse
from subprocess import call, Popen, PIPE
from selex_script import read_fasta_file, read_seq_file, write_fastq_file
from os.path import basename, dirname, join

distance = 5

def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description="Generate cluster seeds")
    parser.add_argument('-i', '--infile', required=False,
                        help="Input FASTQ file")
    parser.add_argument('--count', required=False,
                        help="Input file from fastaptamer_count")
    parser.add_argument('-o', '--outfile', required=True,
                        help="Output file prefix")
    parser.add_argument('-d', '--distance', required=False, default=5,
                        help="Edit distance for clustering")
    parser.add_argument('-f', '--filt', required=False, default=3,
                        help="Read filter")
    parser.add_argument('--clusters', required=False, default=100,
                        help="Maximum number of clusters to find")
    parser.add_argument('--constants', required=False, default=['', ''], nargs=2,
                        help="Constant regions")
    args = parser.parse_args()
    # Enxure that one of the input arguments is provided
    if not(bool(args.infile) ^ bool(args.count)):
        print("Error! Either one of the input arguments must be provided.")
        sys.exit(1)
    infile = args.infile
    count = args.count
    out_pref = args.outfile
    distance = args.distance
    filt = args.filt
    clusters = args.clusters
    constants = args.constants
    
    # Variables
    if infile:
        base_dir = dirname(infile)
        base_name = basename(infile)
    elif count:
        base_dir = dirname(dirname(count))
        base_name = basename(count)
    counts_dir = join(base_dir, 'counts')
    seeds_dir = join(base_dir, 'seeds')
    base_name = base_name[:base_name.rfind('.')]
    #base_name = out_pref
    cluster_file = join(seeds_dir, base_name + '_cluster.fasta')
    out_pref = join(seeds_dir, out_pref)
    
    # Count sequences with fastaptamer_count if we need to
    if args.infile:
        count_file = join(counts_dir, base_name + '_count.fasta')
        print(count_file)
        cmd = ("fastaptamer_count -i {0} -o {1}").format(infile, count_file)
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        out, err = p.communicate()
        if err:
            print("Error! {0}".format(err))
            return
        print(out)
    else:
        count_file = args.count
    
    # Run clustering on the FASTA count file with fastaptamer_cluster.
    cmd = ("fastaptamer_cluster -i {0} -o {1} -d {2} -f {3} "
           "-c {4}").format(count_file, cluster_file, distance, filt, clusters)
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    out, err = p.communicate()
    print(out)
    
    # Extract sequences from the FASTA count file that are cluster seeds.
    # These end in "-1-0", which corresponds to being the #1 sequence in the
    # cluster, at a distance of 0 from the seed.
    cmd = "grep -A1 --no-group-separator -e '\-1\-0$' {0} > {1}".format(
        cluster_file, out_pref + '_seeds.fasta')
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    out, err = p.communicate()
    print(out)
    
    # Use awk to get the sequence file from the seeds, because MPBind requires
    # the input to contain sequences only
    cmd = "awk '{{if (NR % 2 == 0) print}}' {0} > {1}".format(
        out_pref + '_seeds.fasta', out_pref + '_seeds.seq')
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    out, err = p.communicate()
    print(out)
    
    # Add constant regions to the sequences and convert them to FASTQ files.
    # If you run APTANI with cluster seeds, then it requires the full-length
    # sequences.
    if constants[0]:
        full_pref = out_pref.replace('_trim', '')
        cmd = ("selex_helper.py -i {0} -o {1} --add_constants {2} "
               "{3}").format(out_pref + '_seeds.fasta', full_pref + '_seeds_full.fasta',
                             constants[0], constants[1])
        print(cmd)
        p = call(cmd, shell=True)
        cmd = "selex_helper.py -i {0} -o {1} --fasta2fastq".format(
               full_pref + '_seeds_full.fasta', full_pref + '_seeds_full.fastq')
        print(cmd)
        p = call(cmd, shell=True)
        print("Generated full-length FASTQ files for APTANI.")
    
    # Create RNA versions for APTANI.
    cmd = "tr T U < {0} > {1}".format(out_pref + '_seeds.seq',
                                      out_pref + '_seeds_rna.seq')
    p = call(cmd, shell=True)
    
    return
    

if __name__ == "__main__":
    sys.exit(main())
