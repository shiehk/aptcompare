#!/usr/bin/env python

# Calculate sequence frequency in AptCompare

import sys
import argparse
import subprocess
from selex_script import read_fasta_file, read_seq_file, write_fastq_file
from os.path import dirname, join

distance = 5

def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description="Script to calculate sequence frequency in AptCompare")
    parser.add_argument('-i', '--infile', required=True, help="Input file")
    parser.add_argument('-o', '--outfile', required=False, help="Output file")
    args = parser.parse_args()
        
    # File names
    infile = args.infile
    prefix = infile[:infile.rfind('.')]
    outfile = prefix + "_count.fasta"
    if args.outfile:
        outfile = args.outfile
    
    # Check if file is a FASTQ file (by checking file name). Because
    # fastaptamer_count only works on FASTQ files, we convert it to FASTQ if
    # necessary and then run fastaptamer_count.
    ext = infile[infile.rfind('.')+1:]
    if ext.lower() in ['fastq', 'fq']:
        p = subprocess.Popen("fastaptamer_count -i {0} -o {1}".format(infile,
            outfile), stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            shell=True)
        out, err = p.communicate()
        print(out)
        return
    else:
        temp = join(dirname(infile), 'temp')
        if ext.lower() in ['fasta', 'fa']:
            seq_list = read_fasta_file(infile)
            write_fastq_file(temp, seq_list)
        elif ext.lower() in ['sequence', 'seq']:
            seq_list = read_seq_file(infile)
            write_fastq_file(temp, seq_list)
        else:
            print("Error! Unknown file format. Please make sure that the "
                  "file name ends in \'fastq\', \'fasta\', or \'seq\'.")
        temp = temp + '.fastq'
        p = subprocess.Popen("fastaptamer_count -i {0} -o {1}".format(temp,
            outfile), stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            shell=True)
        out, err = p.communicate()
        print(out)
        subprocess.call("rm {0}".format(temp), shell=True)
    return
    

if __name__ == "__main__":
    sys.exit(main())
