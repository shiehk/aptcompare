#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Generation a configuration file for AptaCluster.
#
# By default, the MySQL database configuration is set to 'aptamers'. You can
# change the variables for your configuration.

import sys
import argparse
import subprocess
import os
from os.path import basename, dirname, join

def main(argv=None):
    if argv is None:
        argv = sys.argv
    
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('-0', '--round0', required=True,
                        help="Round 0 (with constants)")
    parser.add_argument('--r0_num', required=True, default='0',
                        help="Initial round number. Default is 0.")
    parser.add_argument('-1', '--round1', required=True,
                        help="Round 1 (with constants)")
    parser.add_argument('--r1_num', required=True,
                        help="Round 1 number")
    parser.add_argument('-2', '--round2', required=True,
                        help="Round 2 (with constants)")
    parser.add_argument('--r2_num', required=True,
                        help="Round 2 number")
    parser.add_argument('-3', '--round3', default = '', required=False,
                        help="Round 3 (with constants). Optional.")
    parser.add_argument('--r3_num', default='', required=False,
                        help="Round 3 number")
    parser.add_argument('-o', '--outdir', required=True,
                        help="Output 'aptacluster' directory")
    parser.add_argument('-f', '--fiveprime', required=True,
                        help="5'-constant region")
    parser.add_argument('-t', '--threeprime', required=True,
                        help="3'-constant region")
    parser.add_argument('-r', '--random_size', required=True,
                        help="Randomized region size")
    parser.add_argument('-m', '--mysql_prefix', required=False,
                        help="MySQL table prefix")
    parser.add_argument('--mysql_db_name', default="aptamers",
                        help="MySQL database name")
    parser.add_argument('--mysql_username', default="aptamers",
                        help="MySQL database username")
    parser.add_argument('--mysql_password', default="aptamers",
                        help="MySQL database password")
    args = parser.parse_args()
    round0 = args.round0
    r0_num = args.r0_num
    round1 = args.round1
    r1_num = args.r1_num
    round2 = args.round2
    r2_num = args.r2_num
    round3 = args.round3
    r3_num = args.r3_num
    outdir = args.outdir
    fp = args.fiveprime
    tp = args.threeprime
    random_size = args.random_size
    mysql_prefix = args.mysql_prefix
    mysql_db_name = args.mysql_db_name
    mysql_username = args.mysql_username
    mysql_password = args.mysql_password
    
    # Set output directory
    base_path = dirname(round0)
    result_path = outdir
    config_file = join(result_path, 'config.cfg')
    
    f = open(config_file, 'w')
    f.write("# AptaCluster Configuration File\n")
    f.write("# For details, please read the AptaCluster config file template\n\n")
    
    # Dataset Specific Options
    f.write("##### Dataset Specific Options #####\n")
    f.write("experiment_description = Aptamer Analysis\n")
    f.write("base_path = {0}/\n".format(base_path))
    f.write("result_path ={0}/\n".format(result_path))
    f.write("file_type = FASTQ\n")
    f.write("is_per_file = true\n\n")
    
    # Control Round
    f.write("##### Round{0} #####\n".format(r0_num))
    f.write("pool_files_forward = {0}\n".format(basename(round0)))
    f.write("number_of_cycles = 1\n")
    f.write("ga2paired_primer_five_prime = {0}\n".format(fp))
    f.write("ga2paired_config_five_prime = {0}\n".format('P' * len(fp)))
    f.write("ga2paired_primer_three_prime = {0}\n".format(tp))
    f.write("ga2paired_config_three_prime = {0}\n".format('P' * len(tp)))
    f.write("cycles = {0}\n".format(r0_num))
    f.write("cycle_ids = R{0}\n".format(r0_num))
    f.write("cycle_is_control = false\n\n")
    
    # Next round
    f.write("##### Round{0} #####\n".format(r1_num))
    f.write("pool_files_forward = {0}\n".format(basename(round1)))
    f.write("number_of_cycles = 1\n")
    f.write("ga2paired_primer_five_prime = {0}\n".format(fp))
    f.write("ga2paired_config_five_prime = {0}\n".format('P' * len(fp)))
    f.write("ga2paired_primer_three_prime = {0}\n".format(tp))
    f.write("ga2paired_config_three_prime = {0}\n".format('P' * len(tp)))
    f.write("cycles = {0}\n".format(r1_num))
    f.write("cycle_ids = R{0}\n".format(r1_num))
    f.write("cycle_is_control = false\n\n")
    
    # Next round
    f.write("##### Round{0} #####\n".format(r2_num))
    f.write("pool_files_forward = {0}\n".format(basename(round2)))
    f.write("number_of_cycles = 1\n")
    f.write("ga2paired_primer_five_prime = {0}\n".format(fp))
    f.write("ga2paired_config_five_prime = {0}\n".format('P' * len(fp)))
    f.write("ga2paired_primer_three_prime = {0}\n".format(tp))
    f.write("ga2paired_config_three_prime = {0}\n".format('P' * len(tp)))
    f.write("cycles = {0}\n".format(r2_num))
    f.write("cycle_ids = R{0}\n".format(r2_num))
    f.write("cycle_is_control = false\n\n")
    
    # Next round (optional)
    if round3 and r3_num:
        f.write("##### Round{0} #####\n".format(r3_num))
        f.write("pool_files_forward = {0}\n".format(basename(round3)))
        f.write("number_of_cycles = 1\n")
        f.write("ga2paired_primer_five_prime = {0}\n".format(fp))
        f.write("ga2paired_config_five_prime = {0}\n".format('P' * len(fp)))
        f.write("ga2paired_primer_three_prime = {0}\n".format(tp))
        f.write("ga2paired_config_three_prime = {0}\n".format('P' * len(tp)))
        f.write("cycles = {0}\n".format(r3_num))
        f.write("cycle_ids = R{0}\n".format(r3_num))
        f.write("cycle_is_control = false\n\n")        
    
    # General information
    f.write("primer_sequence5 = {0}\n".format(fp))
    f.write("primer_sequence3 = {0}\n".format(tp))
    f.write("randomized_region_size = {0}\n\n".format(random_size))
    
    # Quality Control
    f.write("##### Quality Control #####\n")
    f.write("max_mismatch_in_primers = 3\n")
    f.write("ga2paired_maximal_primer_cutoff = 2\n")
    f.write("ga2paired_maximal_number_of_mismatches = 6\n")
    f.write("max_mismatch_in_tags = 1\n\n")
    
    # Database Connection
    f.write("##### Database Connection #####\n")
    f.write("mysql_db_name = {0}\n".format(mysql_db_name))
    f.write("mysql_address = localhost\n")
    f.write("mysql_port = 3306\n")
    f.write("mysql_username = {0}\n".format(mysql_username))
    f.write("mysql_password = {0}\n".format(mysql_password))
    f.write("mysql_table_prefix = {0}\n".format(mysql_prefix))
    f.write("mysql_override_all = true\n")
    f.write("mysql_override_sequence_table = true\n")
    f.write("mysql_override_cluster_table = true\n")
    f.write("mysql_override_cluster_relations_table = true\n")
    f.write("mysql_bulk_insert_sequences = 1000\n\n")
    
    # LSH Options
    f.write("##### Locality Sensitive Hashing Options #####\n")
    f.write("lsh_dimension_percentage = 40\n")
    f.write("lsh_hashing_iterations = 10\n")
    f.write("lsh_sequence_similarity = 10\n")
    f.write("lsh_compute_pvalues = false\n")
    f.write("lsh_pvalue_accuracy = 100\n")
    f.write("kmer_size = 3\n\n")
    
    # Cluster Relations Settings
    f.write("cluster_relations_all_vs_all = true\n")
    f.write("cluster_relations_min_cluster_members = 5\n")
    
    f.close()
    
    # Run AptaCluster
    cmd = ("aptacluster -c {0}".format(config_file))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = p.communicate()
    print(stdout)

if __name__ == "__main__":
    sys.exit(main())
