# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'files/about.ui'
#
# Created: Thu Jun  9 11:09:51 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_AboutWindow(object):
    def setupUi(self, AboutWindow):
        AboutWindow.setObjectName(_fromUtf8("AboutWindow"))
        AboutWindow.resize(400, 300)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("dna-chromosome.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        AboutWindow.setWindowIcon(icon)
        self.textLabel = QtGui.QLabel(AboutWindow)
        self.textLabel.setGeometry(QtCore.QRect(50, 30, 300, 150))
        self.textLabel.setWordWrap(True)
        self.textLabel.setObjectName(_fromUtf8("textLabel"))
        self.closeButton = QtGui.QPushButton(AboutWindow)
        self.closeButton.setGeometry(QtCore.QRect(160, 250, 80, 25))
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.creditsButton = QtGui.QPushButton(AboutWindow)
        self.creditsButton.setGeometry(QtCore.QRect(160, 190, 80, 25))
        self.creditsButton.setObjectName(_fromUtf8("creditsButton"))

        self.retranslateUi(AboutWindow)
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), AboutWindow.close)
        QtCore.QObject.connect(self.creditsButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.creditsButton.show)
        QtCore.QMetaObject.connectSlotsByName(AboutWindow)

    def retranslateUi(self, AboutWindow):
        AboutWindow.setWindowTitle(_translate("AboutWindow", "About AptCompare", None))
        self.textLabel.setText(_translate("AboutWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt;\">AptCompare</span></p><p align=\"center\"><span style=\" font-size:10pt;\">by Kevin Shieh</span></p><p align=\"center\"><span style=\" font-size:10pt;\">If you have any questions or find any bugs, please contact me at kevin [dot] shieh [at] med.einstein.yu.edu.</span></p></body></html>", None))
        self.closeButton.setText(_translate("AboutWindow", "Close", None))
        self.creditsButton.setText(_translate("AboutWindow", "Credits", None))

