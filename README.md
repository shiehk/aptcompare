# AptCompare

AptCompare is a Python-based program that simplifies the analysis of aptamer selections. It provides a graphical user interface that automates the execution of several motif discovery programs and is designed to be easy-to-use for experimentalists and bioinformaticians.

AptCompare contains 6 methods of motif discovery:

* Sequence frequency
* AptaCluster
* APTANI
* FASTAptamer
* MPBind
* RNAmotifAnalysis

More information is provided in the [User Guide](https://drive.google.com/open?id=1Dlu89OxQ_mnL_NR5IVXGutzbRJKOhHIa).

## Pre-Built Images

We have provided virtual machine (VM) images to simplify the build process. There is a 15 GB VirtualBox VM image that you can download, as well as an Amazon Machine Image (AMI) that you can clone into your own directory and run.

### VirtualBox VM

To use the VirtualBox version, download the VM from the link below. In VirtualBox, go to `File > Import Appliance` and select the OVA file.

The username and password for the VirtualBox VM is `aptcompare`. There are a [User Guide](https://drive.google.com/open?id=1Dlu89OxQ_mnL_NR5IVXGutzbRJKOhHIa) and an INSTRUCTIONS file in `~/Documents/` that provide instructions for running the tutorial on the sample data sets. The AptCompare binaries are installed into `~/bin/`.

The full data set is a ZIP file and can be set up in a shared folder. See the user guide for details. The data sets used in the tutorial are subsets of the full data set, with the files in the small data set containing 10,000 sequences each and the files in the large data set containing 500,000 sequences each.

VirtualBox VM (3 GB OVA file, ~15 GB uncompressed, 2 GB RAM): [Link](https://drive.google.com/open?id=11IGpVa-tgj_iSkSN-yZRFWdzOEruagxK)

Full data set (zip file, 139 MB): [Link](https://drive.google.com/open?id=1eSBgzHtVuhsxqkTq5yTzX0XvglA2c9Jn)  
Tutorial data sets (zip file, 110 MB): [Link](https://drive.google.com/open?id=1GgfMk7VXae4fdl3Yh6gccwu5_nqN9Byy)

### AMI

We have also provided an Amazon Machine Image (AMI) that can be cloned and run as your own instance (t2.micro). Free accounts can be created on [Amazon Web Services](https://aws.amazon.com/). In the EC2 Management Console, click on "Images" and then "AMIs" to search for the public AptCompare AMI.  
AMI: `ami-0eeb9972a07686348` [region: US East (Ohio)]

Instructions:

1. In the EC2 Management Console, search for the public AMI (`ami-0eeb9972a07686348`). You may have to set your region to "US East (Ohio)"
2. Launch a new instance
3. Generate a certificate or use your own saved certificate
4. By default, AWS only stores the new key under the `ubuntu` account, but AptCompare runs under the `aptcompare` account. We will force the instance to copy the SSH key to the `aptcompare` account.
>  1. `ssh "[path-to-certificate]" ubuntu@[address]`  
>  2. `tail -1 .ssh/authorized_keys | sudo tee --append /home/aptcompare/.ssh/authorized_keys`  
>  3. `exit`
5. Now we can connect to AptCompare
`ssh -L 5901:localhost:5901 -i "[path-to-certificate]" aptcompare@[address]`
6. If the VNC server is running, you may need to kill it with `vncserver -kill :1`
7. Set your desired screen resolution with `vncserver -geometry 1600x900 -depth 24 :1`
8. In your VNC client, connect to `localhost:5901`


## Building AptCompare

We recommend using the pre-built AptCompare images because they have been tested, but we have also provided step-by-step instructions for building AptCompare if you wish to do so. The instructions are in the file called `INSTALL`.
