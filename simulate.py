#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Generate RNA aptamer data set and simulate selection

import sys
import argparse
import random
from numpy.random import random_integers
from Levenshtein import distance
#import numpy

def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description="Generate RNA data set and simulate selection")
    parser.add_argument('-i', '--infile', required=False, default='',
                        help="Input file")
    parser.add_argument('-o', '--outfile', required=False, default='',
                        help="Output file")
    parser.add_argument('-L', '--length', required=True, default=20, type=int,
                        help="Length of variable region")
    parser.add_argument('-r', '--rounds', required=False, default=0, type=int,
                        help="Number of rounds. Default is 0 (round 0 only).")
    parser.add_argument('-n', '--number', required=False, default=1000, type=int,
                        help="Number of sequences")
    parser.add_argument('-m', '--motif', required=False, default='', type=str,
                        help="Motif sequence. Can be a single motif or multiple ones.")
    args = parser.parse_args()
    infile = args.infile
    outfile = args.outfile
    var_length = args.length
    rounds = args.rounds
    num = args.number
    motif = args.motif
    
    # Constants
    # Sequence generation
    mut_rate = 0.05
    spike_rate = 0.05
    stem = 3
    # Selection
    delta = 0.5         # Selection error rate
    epsilon = 0.05      # Background partition (bg/cp ratio)
    kd = 1e-9           # Kd for the top motif
    
    # Generate aptamers
    seq_list = generate_aptamers(var_length, num)
    # Spike-in a few aptamers with a known motif
    new_data = spike_in_data(seq_list, motif, stem, mut_rate, spike_rate)
    #print(new_data)
    
    # Run selection
    new_data = selex(new_data, 'data', motif, rounds, kd, delta, epsilon)
    

def generate_aptamers(L, N=1000):
    """Generates a random list of N sequences of length L."""
    seqs = list()
    nuc = 'ACGU'
    for i in range(N):
        random.seed()
        indices = random_integers(0, len(nuc)-1, L)
        seqs.append(''.join(map(lambda(j): nuc[j], indices)))
    return seqs

def spike_in_data(seq_list, motif, stem, mut_rate, spike_rate):
    """Spikes data with a motif."""
    # Hairpins only for now
    return spike_in_hairpin(seq_list, motif, stem, mut_rate, spike_rate)

def spike_in_hairpin(seq_list, motif, stem, mut_rate=0.0, spike_rate=1.0):
    """Spikes data with a hairpin."""
    spiked_data = list()
    for i in seq_list:
        r = random.random()
        if r < spike_rate:
            hp = generate_hairpin(stem, motif, mut_rate)
            if len(hp) <= len(i):
                # Insert shape into sequence
                idx = random.randint(0, len(i)-len(hp))
                seq = i[0:idx] + hp + i[idx + len(hp):]
                spiked_data.append(seq)
            else:
                spiked_data.append(i)
        else:
            spiked_data.append(i)
    return spiked_data

def generate_hairpin(stem, motif, mut_rate=0):
    """Generates hairpin, with stem of length stem and a motif in the loop."""
    # Make one side of the stem and call rc to get the reverse complement
    nuc = 'ACGU'
    indices = random_integers(0, len(nuc)-1, stem)
    s5 = ''.join(map(lambda(j): nuc[j], indices))
    s3 = rc(s5)
    if mut_rate:
        temp = list()
        for i in motif:
            r = random.random()
            if r < mut_rate:
                idx = random.randint(0, len(nuc)-1)
                temp.append(nuc[idx])
            else:
                temp.append(i)
        motif = ''.join(temp)
    return s5 + motif + s3

def selex(seq_list, out_prefix, motif='', rounds=1, kd=1e-9, delta=0.2, epsilon=0.05):
    """Simulate SELEX.
    This function  is a simple stochastic model of SELEX. It assumes that
    targets and aptamers bind in a 1:1 ratio, that no non-selective binding by
    the support occurs, and that bound aptamers are not lost in the wash.
    However, it does allow for some error in the selection and partitioning
    steps, defined by the variables delta and epsilon. Epsilon is the bg/cp
    ratio, or the ratio of the percent of background free NAi that is used for
    PCR to the percent of captured taught by the ith NA species.
    
    Epsilon is the bg/cp ratio, where bg is the percent of background free NAi
    and cp is the percent of captured target caught by species NAi
    """
    # TODO (with seq_lisit)
    total = len(seq_list)
    new_seq_list = list(seq_list)
    for i in range(rounds):
        # Simulate selection
        new_seq_list = selection(new_seq_list, motif, kd, delta, epsilon)
        # Simulate PCR amplification
        new_seq_list = amplify(new_seq_list, total)
        write_FASTQ(new_seq_list, out_prefix, i)
    return new_seq_list

def selection(seq_list, motif, kd, delta, epsilon):
    """Simulate each round of the selection step of SELEX."""
    new_seq_list = list()
    for seq in seq_list:
        r = random.random()
        # Check for dissociation
        if r > kd:
            if motif in seq:
                # Selected
                if r > epsilon:   # Allow for some error
                    new_seq_list.append(seq)
            else:
                # Not selected, but we still allow for some error
                if r > (1 - delta):
                    new_seq_list.append(seq)
    return new_seq_list

def amplify(seq_list, total=1e6):
    """Simulate the PCR amplification step of SELEX.
    Note that this function does not change the concentration in each round
    after selection. Instead, we sample the list so that the output has the
    same number of sequences as it did at the beginning of the round, before
    selection."""
    new_seq_list = list(seq_list)
    while len(new_seq_list) < total:
        idx = random.randint(0, len(seq_list)-1)
        new_seq_list.append(seq_list[idx])
    return new_seq_list

def read_FASTA(infile):
    """Reads a FASTA file."""
    seq_list = list()
    f = open(infile, 'r')
    while True:
        line = f.readline().strip()
        if not line:
            break
        else:
            seqid = line[1:]
            seq = f.readline().strip()
            seq_list.append((seqid, seq))
    return seq_list

def write_FASTQ(seq_list, out_prefix, round_num):
    """Writes a FASTQ file."""
    outfile = "{0}_round{1}.fastq".format(out_prefix, round_num)
    ctr = 0
    try:
        f = open(outfile, 'w')
    except IOError:
        print("Error: Unable to open FASTQ file for writing.")
        sys.exit(1)
    for i in seq_list:
        ctr += 1
        f.write("@Seq{0}\n".format(ctr))
        f.write("{0}\n".format(i))
        f.write("+\n")
        f.write("{0}\n".format("I"*len(i)))
    f.close()
    return 0

complement = {'A':'U', 'C':'G', 'G':'C', 'U':'A'}
def rc(s):
    """Returns the reverse complement of a sequence."""
    rc = list()
    for i in s[::-1]:
        rc.append(complement[i])
    return ''.join(rc)

def edit_distance(s1, s2):
    """Simple function to calculate the edit distance (Hamming distance)
    between two sequences of equal length."""
    d = 0
    if len(s1) != len(s2):
        raise ValueError("Sequences are of different lengths!")
    else:
        for i in range(len(s1)):
            if s1[i] != s2[i]:
                d += 1
    return d


if __name__ == "__main__":
    sys.exit(main())
