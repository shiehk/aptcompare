#!/bin/bash

# selex_seed_search.sh
# Search seeds files for selex_covarianceSearch
# Usage: selex_seed_search.sh <cluster number> <cm file> <seeds file> <rounds>

cluster_num=$1
cm_file=$2
seeds_file=$3
rounds=$4

usage="Usage: selex_seed_search.sh <cluster number> <cm file> <seeds file> <rounds>"
if [[ "$#" -eq 1 ]]; then
    if [[ "$1" == '-h' ]] || [[ "$1" == '--help' ]]; then
        echo $usage
    fi
elif [[ "$#" -eq 4 ]]; then
    cp ${cm_file} cluster_${cluster_num}_search_rnd1.cm

    for i in $(seq $rounds); do
        let next=$i+1
    
        echo "#Round ${i}"
        echo "cmsearch --toponly -E 0.1 --tabfile cluster_${cluster_num}_search_rnd${i}.tab cluster_${cluster_num}_search_rnd${i}.cm ${seeds_file}"
        echo "awk '\$1!~/^#/{print \$2}' cluster_${cluster_num}_search_rnd${i}.tab > cluster_${cluster_num}_search_rnd${i}_clusters_found.txt"
        echo "grep -A1 --no-group-separator -w -f cluster_${cluster_num}_search_rnd${i}_clusters_found.txt ${seeds_file} > cluster_${cluster_num}_search_rnd${next}.fasta"
        
        # Align to original CM
        echo "cmalign -1 -o cluster_${cluster_num}_search_rnd${next}_cmaligned.sto ${cm_file} cluster_${cluster_num}_search_rnd${next}.fasta"
        # Align to previous CM
        #echo "cmalign -1 -o cluster_${cluster_num}_search_rnd${next}_cmaligned.sto cluster_${cluster_num}_search_rnd${i}.cm cluster_${cluster_num}_search_rnd${next}.fasta"
        
        echo "cmbuild cluster_${cluster_num}_search_rnd${next}.cm cluster_${cluster_num}_search_rnd${next}_cmaligned.sto"
        echo "cmcalibrate cluster_${cluster_num}_search_rnd${next}.cm"
        echo ""
    done
elif [[ "$#" -eq 5 ]]; then

    # Run searches on reduced CMs (i.e., the smaller CMs)
    sto_file=$5
    cm_prefix=${cm_file%%.cm}

    for i in $(seq $rounds); do
        let next=$i+1
        
        echo "#Round ${i}"
        echo "cmsearch --toponly -E 0.1 --tabfile ${cm_prefix}_rnd${i}.tab ${cm_prefix}_rnd${i}.cm ${seeds_file}"
        echo "awk '\$1!~/^#/{print \$2}' ${cm_prefix}_rnd${i}.tab > ${cm_prefix}_rnd${i}_clusters_found.txt"
        echo "grep -A1 --no-group-separator -w -f ${cm_prefix}_rnd${i}_clusters_found.txt ${seeds_file} > ${cm_prefix}_rnd${next}.fasta"
        
        # Align to original CM
        echo "cmalign -1 -o ${cm_prefix}_rnd${next}_cmaligned.sto ${cm_file} ${cm_prefix}_rnd${next}.fasta"
        # Align to previous CM
        #echo "cmalign -1 -o ${cm_prefix}_rnd${next}_cmaligned.sto ${cm_prefix}_rnd${i}.cm ${cm_prefix}_rnd${next}.fasta"
        
        echo "cmbuild ${cm_prefix}_rnd${next}.cm ${cm_prefix}_rnd${next}_cmaligned.sto"
        echo "cmcalibrate ${cm_prefix}_rnd${next}.cm"
        echo ""
    done
else
    echo $usage
fi
