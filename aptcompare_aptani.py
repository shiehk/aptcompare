#!/usr/bin/env python

# Run APTANI from AptCompare
# 
# APTANI requires constant regions to be given as RNA, so we replace any T's
# with U's. It also requires python 3 to be be used.
#
# This script requires constant regions to be given, as well as a cutoff length.
# python3 ~/bin/APTANI.py --left GGGAAAAGCGAAUCAUACACAAGA --right GGGCAUAAGGUAUUUAAUUCCAUA -x --cutoff 65 ../70HRT14_const.fastq

import sys
import argparse
import os
from subprocess import PIPE

def main(argv=None):
    if argv is None:
        argv = sys.argv
    
    parser = argparse.ArgumentParser(description="Run APTANI from AptCompare")
    parser.add_argument('-i', '--infile', required=True,
                        help="Input FASTQ files")
    parser.add_argument('--c5', required=True,
                        help="5'-constant region")
    parser.add_argument('--c3', required=True,
                        help="3'-constant region")
    parser.add_argument('--cutoff', required=True,
                        help="Cutoff length of the variable region")
    parser.add_argument('-d', '--directory', required=False, default=None,
                        help="Output directory (where script will be executed)")
    parser.add_argument('-s', '--seeds', action='store_true',
                        help="Run analysis on cluster seeds")
    args = parser.parse_args()
    infile = args.infile
    c5 = args.c5.replace('T', 'U')
    c3 = args.c3.replace('T', 'U')
    cutoff = args.cutoff
    directory = args.directory
    seeds = args.seeds
    
    if directory:
        os.chdir(directory)
    
    # If we run APTANI on cluster seeds, we have to change the sampling
    # parameters, or else we will get errors.
    #if seeds:
    #    cmd = ("python3 ~/bin/APTANI.py --left {0} --right {1} -x --cutoff {2} "
    #           "-p 100 -c 20 {3}").format(c5, c3, cutoff, infile)
    #else:
    #    cmd = ("python3 ~/bin/APTANI.py --left {0} --right {1} -x --cutoff {2} "
    #           "-c 20 {3}").format(c5, c3, cutoff, infile)
    #print(cmd)
    #p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
    #                    shell=True)
    #out, err = p.communicate()
    #print(out)
    
    # Run APTANI on all sequences, and then use grep to only keep the seeds.
    # The second command is to combine the header with the sequences.
    cmd = ("python3 ~/bin/APTANI.py --left {0} --right {1} -x --cutoff {2} "
           "{3}").format(c5, c3, cutoff, infile)
    print(cmd)
    #p = subprocess.Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    #out, err = p.communicate()
    #print(out)
    ##subprocess.call(cmd, shell=True)
    os.system(cmd)
    #cmd = ("files=(Hairpins_data.csv Intra_Strand_data.sv Left_Bulges_data.csv "
    #       "Right_Bulges_data.csv)")
    #subprocess.call(cmd, shell=True)
    #cmd = ("for i in \"${files[@]}\";do (head -n 1 ${i}; grep -f "
    #       "../seeds/round5_seeds_rna.seq ${i}) | cat > seeds_${i};done")
    #subprocess.call(cmd, shell=True)
    
    return
    

if __name__ == "__main__":
    sys.exit(main())
