#!/usr/bin/python
# -*- coding: utf-8 -*-

# Main script to run AptCompare.
#
# In order to have AptCompare refresh the output, we force it to update the
# progress bar.

import sys
import pyforms
from pyforms import BaseWidget
from pyforms.Controls import (ControlButton, ControlCheckBox, ControlCombo,
        ControlFile, ControlLabel, ControlText, ControlTextArea, ControlProgress)
from pyforms.gui.standaloneManager import StandAloneContainer
from os import listdir
from os.path import basename, dirname, join, splitext, exists
from shutil import rmtree
from subprocess import call, Popen, PIPE
import operator
import csv
import numpy as np
import pandas as pd
import MySQLdb
from PyQt4 import QtCore
from PyQt4 import QtGui
from PyQt4.QtGui import QPlainTextEdit
import aptcompare_about
import aptcompare_help
import helpmain

# Create global 'win' variable as a reference for a pop-up window; otherwise,
# Python will clean up local variables and close these windows immediately.
win = None

class Comparison(BaseWidget):

    def __init__(self):
        super(Comparison, self).__init__("AptCompare")

        # Set up main menu and provide a shortcut to exit program
        self.mainmenu = [
            {'File': [
                {'Quit Program        Ctrl+Q': self.__runExit}]
             },
            {'Help': [
                {'Help        F1': self.__displayHelp},
                {'About': self.__displayAbout}]
             }
        ]
        # Keyboard shortcuts for the menu
        self.connect(QtGui.QShortcut(QtGui.QKeySequence('Ctrl+Q'), self),
                     QtCore.SIGNAL('activated()'), QtGui.qApp.quit)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence('F1'), self),
                     QtCore.SIGNAL('activated()'), self.__displayHelp)
        
        # Hovering tool tip
        self.setToolTip("Right-click on the individual user interface text "
                        "elements for more information about each item.")

        # Quick Run Tab
        self._quick_file1       = ControlFile("Input File 1 (FASTQ)")
        self._quick_file1.addPopupMenuOption("Choose an input FASTQ file (Required)", self.__dummyEvent)
        self._quick_f1name      = ControlText("Output Prefix", defaultValue="round0")
        self._quick_f1name.addPopupMenuOption("Prefix for output files; recommended: round0", self.__dummyEvent)
        self._quick_file2       = ControlFile("Input File 2 (FASTQ)")
        self._quick_file2.addPopupMenuOption("Choose an input FASTQ file (Required)", self.__dummyEvent)
        self._quick_f2name      = ControlText("Output Prefix", defaultValue="round1")
        self._quick_f2name.addPopupMenuOption("Prefix for output files; recommended: round#", self.__dummyEvent)
        self._quick_file3       = ControlFile("Input File 3 (FASTQ)")
        self._quick_file3.addPopupMenuOption("Choose an input FASTQ file (Required)", self.__dummyEvent)
        self._quick_f3name      = ControlText("Output Prefix", defaultValue="round2")
        self._quick_f3name.addPopupMenuOption("Prefix for output files; recommended: round#")
        self._quick_file4       = ControlFile("Input File 4 (Optional)")
        self._quick_file4.addPopupMenuOption("Choose an input FASTQ file (Optional)", self.__dummyEvent)
        self._quick_f4name      = ControlText("Output Prefix", defaultValue="round3")
        self._quick_f4name.addPopupMenuOption("Prefix for output files; recommended: round#", self.__dummyEvent)
        self._quick_length      = ControlText("VR Length", defaultValue='50')
        self._quick_length.addPopupMenuOption("Length of variable/random region. Default is 50", self.__dummyEvent)
        self._quick_distance    = ControlText("Cluster Distance", defaultValue='5')
        self._quick_distance.addPopupMenuOption("Edit distance for clustering. This defines the maximum mutational distance between cluster seed and members", self.__dummyEvent)
        self._quick_filter      = ControlText("Cluster Filter", defaultValue='5')
        self._quick_filter.addPopupMenuOption("Filter threshold for clustering. This will ignore all sequences with copy numbers below the threshold.", self.__dummyEvent)
        self._quick_name        = ControlText("Unique Name")
        self._quick_name.addPopupMenuOption("Provide a unique, short name for the AptaCluster database of the run", self.__dummyEvent)
        self._quick_count       = ControlCheckBox("Counting", defaultValue=True)
        self._quick_count.addPopupMenuOption("Run sequence frequency or copy number", self.__dummyEvent)
        self._quick_fastapt     = ControlCheckBox("FASTAptamer", defaultValue=True)
        self._quick_fastapt.addPopupMenuOption("Run FASTaptamer analysis", self.__dummyEvent)
        self._quick_aptani      = ControlCheckBox("APTANI", defaultValue=True)
        self._quick_aptani.addPopupMenuOption("Run APTANI analysis", self.__dummyEvent)
        self._quick_mpbind      = ControlCheckBox("MPBind")
        self._quick_mpbind.addPopupMenuOption("Run MPBind analysis", self.__dummyEvent)
        self._quick_aptacl      = ControlCheckBox("AptaCluster")
        self._quick_aptacl.addPopupMenuOption("Run AptaCluster analysis", self.__dummyEvent)
        self._quick_run         = ControlButton("Run all analyses")
        self._quick_run.addPopupMenuOption("Run all of the analyses", self.__dummyEvent)

        # Pre-processing Tab
        self._script_infile     = ControlFile("Input File")
        self._script_infile.addPopupMenuOption("Original input file, which can be in FASTQ, FASTA, or sequence format", self.__dummyEvent)
        self._script_informat   = ControlCombo("Input File Format")
        self._script_informat.addPopupMenuOption("Specify file format of input file", self.__dummyEvent)
        self._script_outfile    = ControlText("Output File Prefix")
        self._script_outfile.addPopupMenuOption("Prefix of output files. Recommended: round#", self.__dummyEvent)
        self._script_outformat  = ControlCombo("Output File Format")
        self._script_outformat.addPopupMenuOption("Specify file format of output file", self.__dummyEvent)
        self._script_allgen     = ControlCheckBox("Generate all formats (FASTQ, FASTA, sequences)", defaultValue=True)
        self._script_allgen.addPopupMenuOption("Generate all formats. Default: Yes", self.__dummyEvent)
        self._script_noambig    = ControlCheckBox("Exclude sequences with ambiguous (i.e., N) nucleotides", defaultValue=True)
        self._script_noambig.addPopupMenuOption("Exclude sequences with ambiguous nucleotides. Default: Yes", self.__dummyEvent)
        self._script_cutadapt   = ControlCheckBox("Remove adapter sequences with cutadapt", defaultValue=True)
        self._script_cutadapt.addPopupMenuOption("Remove adapter sequences with cutadapt", self.__dummyEvent)
        self._script_cutflags   = ControlText("Flags for cutadapt", defaultValue="-e 0.2 -m 45 -M 55")
        self._script_cutflags.addPopupMenuOption("Flags for cutadapt. Recommended: \"-e 0.2 -m [N-5] -M [N+5]\" for 20% error rate, min and max lengths of N-5 and N+5", self.__dummyEvent)
        self._script_run        = ControlButton("Run Pre-Processing")
        self._script_run.addPopupMenuOption("Run pre-processing", self.__dummyEvent)
        # Add items to the combo box
        self._script_informat.addItem("FASTQ", "fastq")
        self._script_informat.addItem("FASTA", "fasta")
        self._script_informat.addItem("Sequence", "sequence")
        self._script_outformat.addItem("FASTQ", "fastq")
        self._script_outformat.addItem("FASTA", "fasta")
        self._script_outformat.addItem("Sequence", "sequence")

        # Cluster Seeds Tab
        self._seeds_infile      = ControlFile("Input File")
        self._seeds_infile.addPopupMenuOption("File type is determined from file extension", self.__dummyEvent)
        self._seeds_outfile     = ControlText("Output File Prefix")
        self._seeds_outfile.addPopupMenuOption("File prefix of output. Recommended: round#", self.__dummyEvent)
        self._seeds_distance    = ControlText("Edit distance for clustering", defaultValue='5')
        self._seeds_distance.addPopupMenuOption("Edit distance. This is the maximum mutational distance between cluster seed and members", self.__dummyEvent)
        self._seeds_filter      = ControlText("Read filter for clustering", defaultValue='5')
        self._seeds_filter.addPopupMenuOption("Ignore all sequences with fewer copy numbers than this threshold", self.__dummyEvent)
        self._seeds_clusters    = ControlText("Maximum number of clusters", defaultValue='50')
        self._seeds_clusters.addPopupMenuOption("Maximum number of clusters to calculate. The higher the number, the longer it will take to run. Default: 50", self.__dummyEvent)
        self._seeds_num         = ControlText("Number of cluster seeds to display", defaultValue='10')
        self._seeds_num.addPopupMenuOption("Top results to display", self.__dummyEvent)
        self._seeds_run         = ControlButton("Generate Cluster Seeds")
        self._seeds_run.addPopupMenuOption("Run clustering", self.__dummyEvent)

        # Counts Tab
        self._count_infile      = ControlFile("Input FASTQ File")
        self._count_infile.addPopupMenuOption("Input file. File extension must be 'fastq', 'fq', 'fasta', 'fa', 'sequence', or 'seq'.", self.__dummyEvent)
        self._count_num         = ControlText("Number of sequences to display", defaultValue='10')
        self._count_num.addPopupMenuOption("Top sequences to display", self.__dummyEvent)
        self._count_run         = ControlButton("Run Counts")
        self._count_run.addPopupMenuOption("Run analysis", self.__dummyEvent)

        # AptaCluster Tab
        self._aptacl_r0         = ControlFile("Initial Round with Constant Regions")
        self._aptacl_r0_name    = ControlText("Round Number")
        self._aptacl_r1         = ControlFile("Next Round with Constants")
        self._aptacl_r1_name    = ControlText("Round Number")
        self._aptacl_r2         = ControlFile("Next Round with Constants")
        self._aptacl_r2_name    = ControlText("Round Number")
        self._aptacl_r3         = ControlFile("Next Round (Opt.) with Constants")
        self._aptacl_r3_name    = ControlText("Round Number")
        self._aptacl_rr_length  = ControlText("Length of Randomized Region")
        self._aptacl_mysql_pref = ControlText("Data Table Prefix (Unique!)")
        self._aptacl_mysql_db   = ControlText("MySQL Database Name", defaultValue="aptamers")
        self._aptacl_mysql_id   = ControlText("MySQL Database Username", defaultValue="aptamers")
        self._aptacl_mysql_pw   = ControlText("MySQL Database Password", defaultValue="aptamers")
        self._aptacl_run        = ControlButton("Run AptaCluster")

        # APTANI Tab
        self._aptani_infile     = ControlFile("Input Full-Length FASTQ or Sequences (with Constants)")
        self._aptani_infile.addPopupMenuOption("Full-length sequences. Must include constant regions for folding prediction", self.__dummyEvent)
        self._aptani_seeds      = ControlCheckBox("File of Cluster Seeds?")
        self._aptani_seeds.addPopupMenuOption("Does this file contain all sequences or cluster seeds only?", self.__dummyEvent)
        self._aptani_cutoff     = ControlText("Variable Region Cutoff Length", defaultValue='50', helptext="Length of variable region")
        self._aptani_cutoff.addPopupMenuOption("Length of variable region, in nucleotides", self.__dummyEvent)
        self._aptani_run        = ControlButton("Run APTANI")
        self._aptani_run.addPopupMenuOption("Run analysis", self.__dummyEvent)

        # FASTAptamer Tab
        self._fastapt_file1     = ControlFile("FASTQ File 1")
        self._fastapt_file1.addPopupMenuOption("FASTQ file only. Required.", self.__dummyEvent)
        self._fastapt_file2     = ControlFile("FASTQ File 2")
        self._fastapt_file2.addPopupMenuOption("FASTQ file only. Required.", self.__dummyEvent)
        self._fastapt_file3     = ControlFile("FASTQ File 3")
        self._fastapt_file3.addPopupMenuOption("FASTQ file only. Required.", self.__dummyEvent)
        self._fastapt_filter    = ControlText("Read Filter", defaultValue='2')
        self._fastapt_filter.addPopupMenuOption("Ignore all sequences with fewer copy numbers than this threshold", self.__dummyEvent)
        self._fastapt_dist      = ControlText("Edit Distance for Clustering", defaultValue='5')
        self._fastapt_dist.addPopupMenuOption("Edit distance. This is the maximum mutational distance between cluster seed and members", self.__dummyEvent)
        self._fastapt_run       = ControlButton("Run FASTAptamer")
        self._fastapt_run.addPopupMenuOption("Run analysis", self.__dummyEvent)

        # MPBind Tab
        self._mpbind_control    = ControlFile("Control Round (Sequences Only!)", helptext="Control round. Required.")
        self._mpbind_control.addPopupMenuOption("Control round. File must contain sequences only. Required.", self.__dummyEvent)
        self._mpbind_file1      = ControlFile("Selection Round 1", helptext="First selection round. Required.")
        self._mpbind_file1.addPopupMenuOption("Selection round 1 containing sequences. Required.", self.__dummyEvent)
        self._mpbind_file2      = ControlFile("Selection Round 2", helptext="Second selection round. Required.")
        self._mpbind_file2.addPopupMenuOption("Selection round 2 containing sequences. Required.", self.__dummyEvent)
        self._mpbind_file3      = ControlFile("Selection Round 3 (Opt.)", helptext="Third selection round. Optional.")
        self._mpbind_file3.addPopupMenuOption("Selection round 3 containing sequences. Optional.", self.__dummyEvent)
        self._mpbind_seeds      = ControlFile("Cluster Seeds File for Searching (Opt., Sequences Only)", helptext="Run MPBind search on a file of cluster seeds. Optional.")
        self._mpbind_seeds.addPopupMenuOption("Cluster seeds file for searching. Run clustering steps first and select the cluster seeds file.", self.__dummyEvent)
        self._mpbind_k          = ControlText("K-mer Length", defaultValue='6')
        self._mpbind_k.addPopupMenuOption("K-mer length of analysis. You can adjust the length and examine results", self.__dummyEvent)
        self._mpbind_unique     = ControlCombo("Unique Reads?")
        self._mpbind_unique.addPopupMenuOption("Include unique reads, redundant reads, or both in analysis?", self.__dummyEvent)
        self._mpbind_run        = ControlButton("Run MPBind")
        self._mpbind_run.addPopupMenuOption("Run analysis", self.__dummyEvent)
        # Combo Box
        self._mpbind_unique.addItem("Unique reads only", 1)
        self._mpbind_unique.addItem("Redundant reads only", 2)
        self._mpbind_unique.addItem("Both unique and redundant reads", 3)

        # RNAmotifAnalysis Tab
        self._rnamotif_infile   = ControlFile("Input File")
        self._rnamotif_infile.addPopupMenuOption("Input file. Can be FASTQ, FASTA, or sequences. Required.", self.__dummyEvent)
        self._rnamotif_outfile  = ControlText("Output File Name")
        self._rnamotif_outfile.addPopupMenuOption("Output file name", self.__dummyEvent)
        self._rnamotif_seeds    = ControlFile("File of Cluster Seeds")
        self._rnamotif_seeds.addPopupMenuOption("File containing cluster seeds in FASTA format, from the \'seeds\' step. Required.", self.__dummyEvent)
        self._rnamotif_rounds   = ControlText("Number of rounds of CM searching and refinement", defaultValue="10")
        self._rnamotif_rounds.addPopupMenuOption("Number of rounds. Default: 10", self.__dummyEvent)
        self._rnamotif_run      = ControlButton("Run RNAmotifAnalysis")
        self._rnamotif_run.addPopupMenuOption("Run analysis", self.__dummyEvent)

        # Results Tab
        self._results_c_label   = ControlLabel("Counts")
        self._results_counts    = ControlFile("Counts File")
        self._results_counts.addPopupMenuOption("File with counts results. Recommended: cluster seeds results from the \'seeds\' analysis", self.__dummyEvent)
        self._results_c_seeds   = ControlCheckBox("Are they cluster seeds?", defaultValue=True)
        self._results_c_seeds.addPopupMenuOption("Check box if these are cluster seeds", self.__dummyEvent)
        self._results_a_label   = ControlLabel("APTANI")
        self._results_aptani    = ControlFile("APTANI File")
        self._results_aptani.addPopupMenuOption("Select any file the APTANI folder for analysis", self.__dummyEvent)
        self._results_a_seeds   = ControlFile("Full-length seed sequences (with constants) for APTANI")
        self._results_a_seeds.addPopupMenuOption("Select full-length cluster seed sequences for APTANI analysis. The file name should end in 'seeds_full.fastq'", self.__dummyEvent)
        self._results_f_label   = ControlLabel("FASTAptamer")
        self._results_fastapt   = ControlFile("FASTAptamer File")
        self._results_fastapt.addPopupMenuOption("FASTAptamer results. The file name ends in '_enrich.tsv'", self.__dummyEvent)
        self._results_f_box     = ControlCombo("FASTAptamer Sorting Method")
        self._results_m_label   = ControlLabel("MPBind")
        self._results_mpbind    = ControlFile("MPBind File")
        self._results_mpbind.addPopupMenuOption("MPBind results. The file name is 'search_results.txt'", self.__dummyEvent)
        self._results_ac_label  = ControlLabel("AptaCluster")
        self._results_aptacl    = ControlFile("AptaCluster Configuration File")
        self._results_aptacl.addPopupMenuOption("AptaCluster file. The file name ends in '_config.txt'", self.__dummyEvent)
        self._results_num       = ControlText("Number of Results to Include", defaultValue="10")
        self._results_num.addPopupMenuOption("Include top N results from each program. Any that are not a result from a program will be ranked N+1", self.__dummyEvent)
        self._results_r_label   = ControlLabel("RNAmotifAnalysis")
        self._results_rnamotif  = ControlFile("RNAmotifAnalysis Final CM Search Tab (*.tab) File")
        self._results_rnamotif.addPopupMenuOption("Final RNAmotifanalysis file ending in '.tab'", self.__dummyEvent)
        self._results_run       = ControlButton("Compile Results")
        self._results_run.addPopupMenuOption("Run results", self.__dummyEvent)
        # Combo Box
        self._results_f_box.addItem("Descending order, R3/R2", 1)
        self._results_f_box.addItem("Descending order, R3/R1", 2)
        #self._results_f_box.addItem("Descending order, Y/X", 3)

        self._constant5         = ControlText("5' Constant Region", defaultValue="GGAGGTGAATGGTTCTACGAT")
        self._constant5.addPopupMenuOption("5' Constant Region adjacent to the variable region", self.__dummyEvent)
        self._constant3         = ControlText("3' Constant Region", defaultValue="TTACATGCGAGATGACCACGT")
        self._constant3.addPopupMenuOption("3' Constant Region. This should be the part immediately adjacent to the variable region.", self.__dummyEvent)
        self._output_log        = ControlTextArea("Output Log")
        self._output_log.addPopupMenuOption("Log of program output", self.__dummyEvent)
        self._progress          = ControlProgress("Idle", defaultValue=0)
        
        # Define the organization of the Form Controls
        # There are two main tabs for "Basic Mode" and "Advanced Mode."
        # Under "Advanced Mode", there are additional tabs for each function.
        self._formset = [{"a:Basic Mode": [("Run a quick analysis with default settings. Use the \"Advanced Mode\" tab for additional configuration options. "
                                            "Right-click on individual user interface text elements for more information about each item."),
                                           ('_quick_file1', '_quick_f1name'),
                                           ('_quick_file2', '_quick_f2name'),
                                           ('_quick_file3', '_quick_f3name'),
                                           ('_quick_file4', '_quick_f4name'),
                                           ('_quick_length', '_quick_distance', '_quick_filter', '_quick_name'),
                                           ('_quick_count', '_quick_fastapt', '_quick_aptani', '_quick_mpbind', '_quick_aptacl'),
                                           '_quick_run'],
                          "b:Advanced Mode": 
                          [{"a:Processing":   [('_script_infile', '_script_informat'),
                                               ('_script_outfile', '_script_outformat'),
                                               ('_script_allgen', '_script_noambig'),
                                               '',
                                               '_script_cutadapt',
                                               ['_script_cutflags', ("Default: -e 0.2 -m 45 -M 55. This means to match constant region with 20% error (~3-4 nt), "
                                                                     "minimum length 45 nt, maximum length 55 nt")],
                                               '_script_run'],
                            "b:Cluster Seeds":['_seeds_infile',
                                               '_seeds_outfile',
                                               '_seeds_distance',
                                               '_seeds_filter',
                                               '_seeds_clusters',
                                               '_seeds_run'],
                            "c:Frequency":    ['_count_infile',
                                               '_count_num',
                                               '_count_run'],                              
                            "d:AptaCluster":  [('_aptacl_r0', '_aptacl_r0_name'),
                                               ('_aptacl_r1', '_aptacl_r1_name'),
                                               ('_aptacl_r2', '_aptacl_r2_name'),
                                               ('_aptacl_r3', '_aptacl_r3_name'),
                                               ('_aptacl_mysql_pref', '_aptacl_rr_length'),
                                               ('_aptacl_mysql_db', '_aptacl_mysql_id', '_aptacl_mysql_pw'),
                                               '_aptacl_run'],
                            "e:APTANI":       ['_aptani_infile',
                                               '_aptani_seeds',
                                               '_aptani_cutoff',
                                               '_aptani_run'],
                            "g:FASTAptamer":  ['_fastapt_file1',
                                               '_fastapt_file2',
                                               '_fastapt_file3',
                                               '_fastapt_filter',
                                               '_fastapt_dist',
                                               'This script calculates counts and enrichment and runs clustering.',
                                               '_fastapt_run'],
                            "h:MPBind":       ['_mpbind_control',
                                               '_mpbind_file1',
                                               '_mpbind_file2',
                                               '_mpbind_file3',
                                               '_mpbind_seeds',
                                               ('_mpbind_k', '_mpbind_unique'),
                                               '_mpbind_run'],
                            "i:RNAmotifAnalysis": ['Run RNAmotifAnalysis. Note that this can take a VERY long period of time!',
                                                   '_rnamotif_infile',
                                                   '_rnamotif_outfile',
                                                   '_rnamotif_seeds',
                                                   '_rnamotif_rounds'],
                            "j:View Results": ['Select output files for analysis. Blank files will be ignored.',
                                               ('_results_counts', '_results_c_seeds'),
                                               '_results_aptacl',
                                               ('_results_aptani', '_results_a_seeds'),
                                               ('_results_fastapt', '_results_f_box'),
                                               '_results_mpbind',
                                               '_results_rnamotif',
                                               '_results_num',
                                               '_results_run'],
                            }]},
                         ('_constant5', '_constant3'),
                         '_output_log', '_progress']

        # Define button actions
        self._quick_run.value = self.__runBasic
        #self._quick_run.value = self.__dummyEvent
        self._script_run.value = self.__runScript
        self._seeds_run.value = self.__runSeeds
        self._count_run.value = self.__runCounts
        self._aptacl_run.value = self.__runAptaCluster
        self._aptani_run.value = self.__runAPTANI
        self._fastapt_run.value = self.__runFASTAptamer
        self._mpbind_run.value = self.__runMPBind
        self._rnamotif_run.value = self.__runRNAmotif
        self._results_run.value = self.__runResults
        

    def __runBasic(self):
        """Basic mode to run all programs."""
        global win
        import time
        self.__addOutput("Running all programs ...\n")
        self._quick_run.label = 'Running ...'
        self.__set_progress(1)
        
        # Testing
        #self._quick_file1.value = '/home/kevin/Documents/Code/comparison/test2/round0-raw.fastq'
        #self._quick_file2.value = '/home/kevin/Documents/Code/comparison/test2/round2-raw.fastq'
        #self._quick_file2.value = '/home/kevin/Documents/Code/comparison/test2/round4-raw.fastq'
        #self._quick_file3.value = '/home/kevin/Documents/Code/comparison/test2/round5-raw.fastq'
        self._quick_file1.value = '/home/kevin/Documents/Code/comparison/htfr_freq/round0-raw.fastq'
        self._quick_file2.value = '/home/kevin/Documents/Code/comparison/htfr_freq/round2-raw.fastq'
        self._quick_file2.value = '/home/kevin/Documents/Code/comparison/htfr_freq/round4-raw.fastq'
        self._quick_file3.value = '/home/kevin/Documents/Code/comparison/htfr_freq/round5-raw.fastq'
        #self._quick_f1name.value = 'round0'
        #self._quick_f2name.value = 'round2'
        #self._quick_f2name.value = 'round4'
        #self._quick_f3name.value = 'round5'
        #self._quick_name.value = 'htfr'
        
        # Shared variables for programs
        var_length = int(self._quick_length.value)
        edit_distance = int(self._quick_distance.value)
        filter_thresh = int(self._quick_filter.value)
        constant5 = self._constant5.value
        constant3 = self._constant3.value
        
        # Lists to store file names and file prefixes. First we check if the
        # files exist, and store them into separate lists. The output file list
        # only contains prefixes because the programs sometimes require FASTQ,
        # FASTA, or sequence files.
        # infile_list - original input files (FASTQ)
        # outpref_list - prefixes of outputs
        # outpref_trim_list - prefixes of trimmed outputs (CRs removed)
        # seeds_file - cluster seeds of last round
        # file_dir - main directory for all the data
        infile_list = list()
        outpref_list = list()
        outpref_trim_list = list()
        seeds_file = None
        file_dir = None
        if all([self._quick_file1.value, self._quick_f1name.value,
                self._quick_file2.value, self._quick_f2name.value,
                self._quick_file3.value, self._quick_f3name.value]):
            infile_list = [self._quick_file1.value, self._quick_file2.value,
                           self._quick_file3.value]
            outpref_list = [self._quick_f1name.value, self._quick_f2name.value,
                            self._quick_f3name.value]
        else:
            self.__addOutput("ERROR! Input files and output file prefixes cannot be blank!")
            self._quick_run.label = 'Run all analyses'
            self.__set_progress(0)
            return
        if self._quick_file4.value:
            infile_list.append(self._quick_file4.value)
            outpref_list.append(self._quick_f4name.value)
        file_dir = dirname(self._quick_file1.value)
        outpref_trim_list = map(lambda x: x + '_trim', outpref_list)
        # Check arguments
        if not self._quick_name.value:
            self.__addOutput("ERROR! Please enter a short, unique name for the experiment!")
            self._quick_run.label = 'Run all analyses'
            self.__set_progress(0)
            return
        if not all([self._quick_length.value, self._quick_distance.value]):
            self.__addOutput("ERROR! Missing required inputs!")
            self._quick_run.label = 'Run all analyses'
            self.__set_progress(0)
            return
        if not any([self._quick_count.value, self._quick_fastapt.value,
                    self._quick_aptani.value, self._quick_mpbind.value,
                    self._quick_aptacl.value]):
            self.__addOutput("ERROR! At least one of the programs must be selected!")
            self._quick_run.label = 'Run all analyses'
            self.__set_progress(0)
            return
        exp_name = self._quick_name.value.replace(' ', '_')
        
        # Progress bar
        # Add 2 to the total number of analyses because of pre-processing and
        # clustering.
        num_analyses = sum([bool(self._quick_count.value),
                            bool(self._quick_fastapt.value),
                            bool(self._quick_aptani.value),
                            bool(self._quick_mpbind.value),
                            bool(self._quick_aptacl.value)]) + 2
        progress_step = 80.0 / num_analyses
        progress_ctr = 0
        self.__set_progress(5)
        
        # Pre-processing
        # Generates all necessary files, assuming constant regions are given
        # By default, we use a 20% error rate for cutadapt and set minimum
        # and maximum thresholds as n-5 and n+5 if variable region length is n
        # Input files should be in FASTQ format, but we guess the file type
        # anyway from the file extension.
        self.__addOutput("Running pre-processing step ...")
        cut_flags = "-e 0.2 -m {0} -M {1}".format(var_length - 5, var_length + 5)
        for i in range(len(infile_list)):
            # Check file extension
            file_ext = splitext(infile_list[i])[1][1:].lower()
            if file_ext in ['fastq', 'fq']:
                in_ext = 'fastq'
            elif file_ext in ['fasta', 'fa']:
                in_ext = 'fasta'
            elif file_ext in ['sequence', 'seq']:
                in_ext = 'sequence'
            else:
                self.__addOutput("ERROR! Input files should have FASTQ, FASTA, or sequence file extensions!")
                self._quick_run.label = 'Run all analyses'
                self.__set_progress(0)
                return
            cmd = ("selex_script.py -i {0} --informat {1} "
                   "-o {2} --outformat fastq --allgen -c {3} {4} --cutadapt "
                   "--cutadapt_flags {5}").format(infile_list[i], in_ext,
                                                  outpref_list[i], constant5, constant3, cut_flags)
            self.__addOutput(cmd)
            p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
            out, err = p.communicate()
            print(out)
            print(err)
            self.__split_stdout(out)
        self.__addOutput("Finished pre-processing step!\n")
        progress_ctr += 1
        self.__set_progress(int(progress_ctr * progress_step) + 5)
        
        # Cluster the last round of the selection to get seeds
        self.__addOutput("Running clustering step ...")
        out_dir = join(file_dir, 'seeds')
        call("mkdir {0}".format(out_dir), shell=True)
        temp_in = join(file_dir, outpref_trim_list[-1] + '.fastq')
        seeds_pref = join(out_dir, outpref_trim_list[-1])
        seeds_file = seeds_pref + '_seeds.fasta'
        self.__addOutput("Clustering file: {0}".format(temp_in))
        cmd = ("cluster_seeds.py -i {0} -o {1} -d {2} -f {3} "
               "--clusters 300 --constants {4} {5}").format(temp_in,
                    seeds_pref, edit_distance, filter_thresh,
                    constant5, constant3)    ##### Change filter value back to 5 later
        self.__addOutput(cmd)
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        out, err = p.communicate()
        self.__split_stdout(out)
        self.__addOutput("Generated cluster seeds\n")
        progress_ctr += 1
        self.__set_progress(int(progress_ctr * progress_step) + 5)
        
        # Count sequences
        if self._quick_count.value:
            self.__addOutput("Running counting step ...")
            out_dir = join(file_dir, 'counts')
            call("mkdir {0}".format(out_dir), shell=True)
            for i in outpref_trim_list:
                temp_in = join(file_dir, i + '.fastq')
                temp_out = join(out_dir, i + '_count.fasta')
                self.__addOutput("Creating file: {0}".format(temp_out))
                cmd = "aptcompare_count.py -i {0} -o {1}".format(temp_in, temp_out)
                self.__addOutput(cmd)
                p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
                out, err = p.communicate()
                self.__split_stdout(out)
            self.__addOutput("Finished counting sequences\n")
            progress_ctr += 1
            self.__set_progress(int(progress_ctr * progress_step) + 5)
        
        # FASTAptamer
        if self._quick_fastapt.value:
            self.__addOutput("Running FASTAptamer ...")
            out_dir = join(file_dir, 'fastaptamer')
            call("mkdir {0}".format(out_dir), shell=True)
            temp_list = map(lambda x: join(file_dir, x), outpref_trim_list)
            cmd = ("aptcompare_fastaptamer.py {1} {2} -d {3} -f {4} "
                   "--seeds {5}").format(temp_list[0] + '.fastq',
                        temp_list[1] + '.fastq', temp_list[2] + '.fastq',
                        edit_distance, filter_thresh, seeds_file) ##### Change filter value back to 5
            self.__addOutput(cmd)
            p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
            out, err = p.communicate()
            self.__split_stdout(out)
            self.__addOutput("Finished running FASTAptamer\n")
            progress_ctr += 1
            self.__set_progress(int(progress_ctr * progress_step) + 5)
        
        # APTANI
        # Run on all sequence files but filter for cluster seeds
        # Seeds file should be RNA sequences        
        if self._quick_aptani.value:
            self.__addOutput("Running APTANI ...")
            out_dir = join(file_dir, 'aptani')
            if exists(out_dir):
                rmtree(out_dir)
            call("mkdir {0}".format(out_dir), shell=True)
            input_fastq = join(file_dir, outpref_list[-1] + '.fastq')
            cmd = ("aptcompare_aptani.py --c5 {1} --c3 {2} --cutoff {3} "
               "-d {4}").format(input_fastq, constant5, constant3,
                                            var_length - 5, out_dir)
            self.__addOutput(cmd)
            p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
            out, err = p.communicate()
            self.__split_stdout(out)
            results_aptani = join(out_dir, 'Hairpin_data.csv')
            self.__addOutput("Finished running APTANI\n")
            progress_ctr += 1
            self.__set_progress(int(progress_ctr * progress_step) + 5)
        
        # MPBind
        # By default, use k = 6. MPBind_Predict.py requires seed sequences
        if self._quick_mpbind.value:
            self.__addOutput("Running MPBind ...")
            out_dir = join(file_dir, 'mpbind')
            call("mkdir {0}".format(out_dir), shell=True)
            input_seq = map(lambda x: join(file_dir, x + '.seq'), outpref_trim_list)
            seeds_seq = seeds_pref + '_seeds.seq'
            cmd = ("aptcompare_mpbind.py --R0 {0} --RS {1} -k {2} -o {3} -u {4} "
                   "-s {5}").format(input_seq[0], ','.join(input_seq[1:]), 6,
                                    join(out_dir, 'MPBind_Out6'), 1, seeds_seq)
            self.__addOutput(cmd)
            p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
            out, err = p.communicate()
            self.__split_stdout(out)
            results_mpbind = join(out_dir, 'MPBind_Out6', 'search_results.txt')
            self.__addOutput("Finished running MPBind")
            progress_ctr += 1
            self.__set_progress(int(progress_ctr * progress_step) + 5)
        
        # AptaCluster
        if self._quick_aptacl.value:
            self.__addOutput("Running AptaCluster ...")
            out_dir = join(file_dir, 'aptacluster')
            call("mkdir {0}".format(out_dir), shell=True)
            input_list = map(lambda x: join(file_dir, x + '.fastq'), outpref_list)
            round_num_list = map(lambda x: x.lower().replace('round', ''), outpref_list)
            if len(input_list) == 3:
                cmd = ("aptcompare_aptacluster.py -0 {0} --r0_num {1} "
                       "-1 {2} --r1_num {3} -2 {4} --r2_num {5} "
                       "-o {6} -f {7} -t {8} -r {9} -m {10} "
                       "--mysql_db_name aptamers --mysql_username aptamers "
                       "--mysql_password aptamers").format(
                           input_list[0], round_num_list[0], input_list[1],
                           round_num_list[1], input_list[2], round_num_list[2],
                           out_dir, constant5, constant3, self._quick_length,
                           self._quick_name)
            elif len(input_list) == 4:
                cmd = ("aptcompare_aptacluster.py -0 {0} --r0_num {1} "
                       "-1 {2} --r1_num {3} -2 {4} --r2_num {5} -3 {6} --r3_num "
                       "{7} -o {8} -f {9} -t {10} -r {11} -m {12} "
                       "--mysql_db_name aptamers --mysql_username aptamers "
                       "--mysql_password aptamers").format(
                           input_list[0], round_num_list[0], input_list[1],
                           round_num_list[1], input_list[2], round_num_list[2],
                           input_list[3], round_num_list[3], out_dir, constant5,
                           constant3, self._quick_length, self._quick_name)
            self.__addOutput(cmd)
            p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
            out, err = p.communicate()
            self.__split_stdout(out)
            results_aptacl = join(out_dir, 'config.cfg')
            self.__addOutput("Finished running AptaCluster")
            progress_ctr += 1
            self.__set_progress(int(progress_ctr * progress_step) + 5)
        
        ##### Results #####
        # Shows top 15 results from each program
        # Skip RNAmotifAnalysis because it will take too long to run
        # The results are stored in the following variables:
        #   results_counts - equivalent to cluster seeds
        #   results_fastapt
        #   results_aptani
        #   results_mpbind
        #   results_aptacl
        results_counts = seeds_file
        num = 15
        self.__set_progress(85)
        
        # Count sequences
        if self._quick_count.value:
            lst = self.__read_fastaptamer(results_counts, 'cluster', num)
            counts = list()
            for i in lst:
                temp = [i[0]]
                temp.extend(map(float, i[1]))
                counts.append(temp)
            temp = map(lambda x: [x[0], x[4]], counts)
            ranked_counts = sorted(temp, key=operator.itemgetter(1))
        
        # FASTAptamer
        if self._quick_fastapt.value:
            fastapt_dir = join(file_dir, 'fastaptamer')
            seeds_files = listdir(fastapt_dir)
            for f in seeds_files:
                if f.endswith('.tsv'):
                    results_fastapt = join(fastapt_dir, f)
            lst, header = self.__read_fastaptamer_enrich(results_fastapt, num)
            temp = pd.DataFrame(lst, columns=header, dtype='float')
            df = temp[temp['Rank in Cluster (z)'] == 1] # Select cluster seeds
            if self._results_f_box.value == 1:      # Sort Z/Y
                df.sort('Enrichment (z/y)', ascending=False, inplace=True)
            elif self._results_f_box.value == 2:    # Sort Z/X
                df.sort('Enrichment (z/x)', ascending=False, inplace=True)
            ranked_fastapt = list()
            for rnk, seq in enumerate(df['Sequence'][:num]):
                ranked_fastapt.append([seq, float(rnk + 1)])
        
        # APTANI
        if self._quick_aptani.value:
            results_aptani = join(file_dir, 'aptani', 'Hairpins_data.csv')
            aptani_dir = dirname(results_aptani)
            seeds_full_file = seeds_file.replace('_trim_seeds', '_seeds_full')
            if num >= 15:
                temp = self.__read_aptani(aptani_dir, seeds_full_file, 50,
                                          self._constant5.value,
                                          self._constant3.value)
            else:
                temp = self.__read_aptani(aptani_dir, seeds_full_file, 15,
                                          self._constant5.value,
                                          self._constant3.value)
            # Re-sort data
            temp.sort(['Frequency', 'Identity'], ascending=[0,0], inplace=True)
            # Clean up data because the strings have extra spaces. Also delete constant regions.
            aptani = list(temp.iloc[:, 5])
            ranked_aptani = list()
            for i in range(len(aptani)):
                temp = aptani[i].strip().upper().replace('U', 'T')
                temp = temp.replace(self._constant5.value, '').replace(self._constant3.value, '').replace(self._constant3.value[:-2], '')
                ranked_aptani.append([temp, 1.0 * (i + 1)])
            if len(ranked_aptani) > num:
                ranked_aptani = ranked_aptani[:num]
        
        # MPBind
        if self._quick_mpbind.value:
            ranked_mpbind = self.__read_mpbind(results_mpbind, num)
        
        # AptaCluster
        if self._quick_aptacl.value:
            d = self.__parse_config(results_aptacl)
            db = MySQLdb.connect(host="localhost",
                                 user=d['mysql_username'],
                                 passwd=d['mysql_password'],
                                 db=d['mysql_db_name'])
            c = db.cursor()
            table_name = d['mysql_table_prefix']
            rounds = d['cycle_ids']     # Get last cycle_ids
            # Get most abundant clusters from final round.
            c.execute("SELECT * FROM {0} ORDER BY size DESC LIMIT 10;".format(
                table_name + '_cluster_meta_' + rounds))
            clusters = c.fetchall()
            ranked_aptacluster = list()
            for i in range(len(clusters)):
                tup = clusters[i]
                cid = tup[-1]
                c.execute("SELECT * FROM {0} where id={1};".format(
                    table_name + '_sequences_' + rounds, cid))
                nuc = c.fetchone()
                ranked_aptacluster.append([nuc[1], 1.0 * (i + 1)])
        
        # Use Pandas to merge lists
        df_all = list()
        headers = ['Cluster Seed']
        total = 0
        if self._quick_count.value:
            df_c = pd.DataFrame(ranked_counts, columns=['Sequences', 'Counts'])
            df_all.append(df_c)
            headers.append('Frequency')
            total += 1
        if self._quick_aptani.value:
            df_apt = pd.DataFrame(ranked_aptani, columns=['Sequences', 'APTANI'])
            df_all.append(df_apt)
            headers.append('APTANI')
            total += 1
        if self._quick_fastapt.value:
            df_f= pd.DataFrame(ranked_fastapt, columns=['Sequences', 'FASTAptamer'])
            df_all.append(df_f)
            headers.append('FASTAptamer')
            total += 1
        if self._quick_mpbind.value:
            df_m = pd.DataFrame(ranked_mpbind, columns=['Sequences', 'MPBind'])
            df_all.append(df_m)
            headers.append('MPBind')
            total += 1
        if self._quick_aptacl.value:
            df_ac = pd.DataFrame(ranked_aptacluster, columns=['Sequences', 'AptaCluster'])
            df_all.append(df_ac)
            headers.append('AptaCluster')
            total += 1
        df = reduce(lambda left, right: pd.merge(left, right, on='Sequences', how='outer'), df_all)
        
        self.__set_progress(95)
        
        # Count number of programs that rank a sequence (e.g., 4 out of 6
        # selected this sequence). Also calculate the mean ranks, and replace
        # NaNs with a numeric value.
        appear = total - df.isnull().sum(axis=1)    # Count NaNs before replacing
        df = df.fillna(1.0 * (num+1))
        weights = df.mean(axis=1)
        df.loc[:, 'Ranks'] = weights
        headers.append('Average Rank')
        df.loc[:, 'Appearances'] = appear
        headers.append('Appearances')
        
        # Convert back to a list of lists and cast numbers as floats, then
        # display in the results window. Set upper bound of ranks as the number
        # of motifs plus 1.
        lol = list()
        for i in range(len(df)):
            lol.append(list(df.loc[i]))
            ranks = list()
        for i in lol:
            temp = [i[0]]
            temp.extend([num+1 if np.isnan(x) else float(x) for x in i[1:]])
            ranks.append(temp)
        
        self.__addOutput("Done!")
        self.__set_progress(100)
        win = ResultsWindow(ranks, headers)
        win.show()
        
        # Completed
        self._quick_run.label = 'Run all analyses'
        self.__set_progress(0)
        
        return

    def __runScript(self):
        """Run selex_script.py"""
        self.__addOutput("Running selex_script.py ...")
        self._quick_run.label = 'Running ...'
        self.__set_progress(5)
        
        if self._script_allgen.value:
            allgen = '--allgen'
        else:
            allgen = ''
        if self._script_cutadapt:
            cmd = ("selex_script.py -i {0} --informat {1} "
                   "-o {2} --outformat {3} {4} -c {5} {6} --cutadapt "
                   "--cutadapt_flags {7}").format(
                       self._script_infile.value, self._script_informat.value,
                       self._script_outfile.value, self._script_outformat.value,
                       allgen, self._constant5.value,
                       self._constant3.value, self._script_cutflags.value)
            self.__addOutput(cmd)
            p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
            out, err = p.communicate()
            self.__split_stdout(out)
            self.__addOutput("Finished removing adapters")
        else:
            cmd = ("selex_script.py -i {0} --informat {1} "
                   " -o {2} --outformat {3} {4}").format(
                       self._script_infile.value, self._script_informat.value,
                       self._script_outfile.value, self._script_outformat.value,
                       allgen)
            self.__addOutput(cmd)
            p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
            out, err = p.communicate()
            self.__split_stdout(out)
            self.__addOutput("Finished generating files")
        self.__addOutput("Finished running script!")
        self._script_run.label = 'Run Pre-Processing'
        self.__set_progress(0)

    def __runCounts(self):
        """Run aptcompare_count.py
        We will create a 'counts' subdirectory. The output file will be
        created in this subdirectory with '_count_fasta' in its name.
        """
        global win
        if not self._count_infile.value:
            self.__addOutput("Error! Input file is empty")
            return
        self.__addOutput("Counting sequences ...")
        self._quick_run.label = 'Running ...'
        self.__set_progress(5)

        # Create subdirectory
        out_dir = dirname(self._count_infile.value)
        out_dir = join(out_dir, 'counts')
        call("mkdir {0}".format(out_dir), shell=True)
        file_name = basename(self._count_infile.value)
        file_name = file_name[:file_name.rfind('.')]
        outfile = join(out_dir, file_name + '_count.fasta')
        self.__addOutput("Creating file: {}".format(outfile))

        # Run aptcompare_count.py (which uses FASTAptamer)
        cmd = "aptcompare_count.py -i {0} -o {1}".format(self._count_infile.value,
                                                          outfile)
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        stdout, stderr = p.communicate()
        self.__split_stdout(stdout)
        self.__addOutput("Finished counting sequences!")
        self.__set_progress(100)

        # Display results
        # We read the output from fastaptamer_count and convert the output
        # into one list, keeping all numeric types as floats for proper
        # sorting.
        lst = self.__read_fastaptamer(outfile, 'count', int(self._count_num.value))
        new_lst = list()
        for i in lst:
            temp = [i[0]]
            temp.extend(map(float, i[1]))
            new_lst.append(temp)
        win = ResultsWindow(new_lst, ['Sequence', 'Rank', 'Count', 'RPM'])
        win.show()
        self._quick_run.label = 'Run Counts'
        self.__set_progress(0)

    def __runAptaCluster(self):
        """Run aptcompare_aptacluster.py"""
        self.__addOutput("Running AptaCluster ...")
        self._quick_run.label = 'Running ...'
        self.__set_progress(5)

        # Create subdirectory
        out_dir = dirname(self._aptacl_r0.value)
        out_dir = join(out_dir, 'aptacluster')
        call("mkdir {0}".format(out_dir), shell=True)

        # Run AptaCluster
        cmd = ("aptcompare_aptacluster.py -0 {0} --r0_num {1} "
               "-1 {2} --r1_num {3} -2 {4} --r2_num {5} {6} {7} "
               "-o {8} -f {9} -t {10} -r {11} -m {12} --mysql_db_name {13} "
               "--mysql_username {14} --mysql_password {15}").format(
                   self._aptacl_r0.value, self._aptacl_r0_name.value,
                   self._aptacl_r1.value, self._aptacl_r1_name.value,
                   self._aptacl_r2.value, self._aptacl_r2_name.value,
                   "-3 " + self._aptacl_r3.value,
                   "--r3_num " + self._aptacl_r3_name.value,
                   out_dir,
                   self._constant5.value,
                   self._constant3.value,
                   self._aptacl_rr_length.value,
                   self._aptacl_mysql_pref.value,
                   self._aptacl_mysql_db.value,
                   self._aptacl_mysql_id.value,
                   self._aptacl_mysql_pw.value)
        self.__addOutput(cmd)
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        stdout, stderr = p.communicate()
        self.__split_stdout(stdout)
        self.__addOutput("Finished running AptaCluster")
        self.__set_progress(95)

        # Set up variables
        table_name = self._aptacl_mysql_pref.value
        if self._aptacl_r3.value and self._aptacl_r3_name.value:
            rounds = [self._aptacl_r0_name.value, self._aptacl_r1_name.value,
                      self._aptacl_r2_name.value, self._aptacl_r3_name.value]
        else:
            rounds = [self._aptacl_r0_name.value, self._aptacl_r1_name.value,
                      self._aptacl_r2_name.value]
        last_round = rounds[-1]

        # Connect to MySQL database
        db = MySQLdb.connect(host="localhost",
                             user=self._aptacl_mysql_id.value,
                             passwd=self._aptacl_mysql_pw.value,
                             db=self._aptacl_mysql_db.value)
        c = db.cursor()

        # Display results. Get most abundant clusters from final round.
        c.execute("SELECT * FROM {0} ORDER BY size DESC LIMIT 10;".format(
            table_name + '_cluster_meta_' + rounds[-1]))
        clusters = c.fetchall()
        ranks = list()
        for tup in clusters:
            cid = tup[-1]
            c.execute("SELECT * FROM {0} where id={1};".format(
                table_name + '_sequences_' + rounds[-1], cid))
            nuc = c.fetchone()
            ranks.append(nuc[1])
        print(ranks)
        self._quick_run.label = 'Run AptaCluster'
        self.__set_progress(0)

    def __runAPTANI(self):
        """Run aptcompare_aptani.py"""
        self.__addOutput("Running APTANI ...")

        # Create subdirectory. If the input file is a cluster seeds file, then
        # we call 'dirname' twice to get the output directory.
        if self._aptani_seeds.value:
            out_dir = dirname(dirname(self._aptani_infile.value))
        else:
            out_dir = dirname(self._aptani_infile.value)
        out_dir = join(out_dir, 'aptani')
        call("mkdir {0}".format(out_dir), shell=True)

        # Run APTANI
        seeds_flag = '--seeds' if self._aptani_seeds.value else ''
        cmd = ("aptcompare_aptani.py -i {0} --c5 {1} --c3 {2} --cutoff {3} "
               "{4} -d {5}").format(self._aptani_infile.value, self._constant5.value,
                                    self._constant3.value, self._aptani_cutoff.value,
                                    seeds_flag, out_dir)
        self.__addOutput(cmd)
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        out, err = p.communicate()
        self.__split_stdout(out)
        self.__addOutput("Finished running APTANI!")

        # Display results

    def __runFASTAptamer(self):
        """Runs aptcompare_fastaptamer.py"""
        self.__addOutput("Running FASTAptamer ...")

        # Create subdirectory
        out_dir = dirname(self._fastapt_file1.value)
        out_dir = join(out_dir, 'fastaptamer')
        call("mkdir {0}".format(out_dir), shell=True)

        # Check that at least 3 inputs are given and run FASTAptamer
        if self._fastapt_file1.value and self._fastapt_file2.value and self._fastapt_file3.value:
            cmd = ("aptcompare_fastaptamer.py {1} {2} -d {3} -f "
                   "{4}").format(self._fastapt_file1.value, self._fastapt_file2.value,
                                 self._fastapt_file3.value, self._fastapt_dist.value,
                                 self._fastapt_filter.value)
            self.__addOutput(cmd)
            p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
            out, err = p.communicate()
            self.__split_stdout(out)
            self.__addOutput("Finished running FASTAptamer")
        else:
            self.__addOutput("Error! FASTAptamer input file required")

    def __runMPBind(self):
        """Runs aptcompare_mpbind.py
        aptcompare_mpbind.py --R0 round0_trim.seq --RS round4_trim.seq,round5_trim.seq -k 6 -o MPBind_Out -u 1 -s round5_trim.seq
        """
        self.__addOutput("Running MPBind ...")

        # Get dirname with os.path.dirname()
        # Create mpbind subdirectory and add that to output directory
        out_dir = dirname(self._mpbind_control.value)
        out_dir = join(out_dir, 'mpbind')
        call("mkdir {0}".format(out_dir), shell=True)

        # Check that at least 3 files are given and run MPBind
        if not all([self._mpbind_control.value, self._mpbind_file1.value, self._mpbind_file2.value]):
            self.__addOutput("Error! At least 3 input files are required!")
            self.__addOutput("Include the control file and at least 2 selection rounds.")
            return
        RS = [self._mpbind_file1.value, self._mpbind_file2.value]
        if self._mpbind_file3.value:
            RS.append(self._mpbind_file3.value)
        out_dir = join(out_dir, 'MPBind_Out' + self._mpbind_k.value)
        cmd = ("aptcompare_mpbind.py --R0 {0} --RS {1} -k {2} -o {3} -u {4} "
               "-s {5}").format(self._mpbind_control.value, ','.join(RS),
                                self._mpbind_k.value, out_dir,
                                self._mpbind_unique.value, self._mpbind_seeds.value)
        self.__addOutput(cmd)
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        out, err = p.communicate()
        self.__addOutput(err)
        self.__split_stdout(out)
        self.__addOutput("Finished running MPBind!")

    def __runRNAmotif(self):
        """Runs aptcompare_rnamotif.py for RNAMotifAnalysis"""
        self.__addOutput("Running RNAmotifAnalysis...")
        self.__addOutput("Depending on the number of rounds and the size of the library, this may take a long time to complete.")

        # Create subdirectory
        out_dir = dirname(self._rnamotif_infile.value)
        out_dir = join(out_dir, 'rnamotif')
        call("mkdir {0}".format(out_dir), shell=True)
        outfile = join(_dir, self_rnamotif_outfile)

        cmd = ("aptcompare_rnamotif.py -i {0} -o {1} -s {2} --c5 {3} --c3 "
               "{4}".format(self._rnamotif_infile, outfile,
                            self._rnamotif_seeds, self._constant5,
                            self._constant3))
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        out, err = p.communicate()
        self.__addOutput(out)
        self.__addOutput("Finished running RNAmotifAnalysis!")
        return

    def __runSeeds(self):
        """Generates a file containing the most abundant sequence in each
        clusters. Uses fastaptamer_count for counting sequences and
        fastaptamer_cluster for clustering."""
        
        self._seeds_infile.value = '/home/kevin/Documents/Code/comparison/htfr/freqdist/round5_trim.fastq'
        self._seeds_outfile.value = 'round5_trim'
        global win
        self._seeds_run.label = 'Running ...'
        self.__set_progress(1)
        
        if not self._seeds_infile.value:
            self.__addOutput("Error! Input file is empty")
            self._seeds_run.label = 'Run clustering'
            self.__set_progress(0)
            return    
        self.__addOutput("Determining cluster seeds ...")
        
        # Create subdirectories for counts and seeds
        out_dir = dirname(self._seeds_infile.value)
        count_dir = join(out_dir, 'counts')
        call("mkdir {0}".format(count_dir), shell=True)
        seeds_dir = join(out_dir, 'seeds')
        call("mkdir {0}".format(seeds_dir), shell=True)
        file_name = basename(self._seeds_infile.value)
        file_name = file_name[:file_name.rfind('.')]
        count_file = join(count_dir, file_name + '_count.fasta')
        self.__addOutput("Creating file: {}".format(count_file))
        #cluster_file = join(seeds_dir, self._seeds_outfile.value + "_clusters.fasta")
        #out_pref = join(seeds_dir, self._seeds_outfile.value + "_seeds")
        out_pref = self._seeds_outfile.value

        # Run aptcompare_count.py (which uses FASTAptamer)
        #cmd = "aptcompare_count.py -i {0} -o {1}".format(self._seeds_infile.value,
        #                                                  count_file)
        #p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        #out, err = p.communicate()
        #self.__split_stdout(out)

        # Then run cluster_seeds.py to determine clusters and extract seeds
        self.__addOutput("Running clustering ...")
        cmd = ("cluster_seeds.py -i{0} -o {1} -d {2} -f {3} "
               "--clusters {4}").format(self._seeds_infile.value, out_pref,
                                        self._seeds_distance.value,
                                        self._seeds_filter.value,
                                        self._seeds_clusters.value)
        self.__addOutput(cmd)
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        out, err =  p.communicate()
        self.__split_stdout(out)
        self.__addOutput("Generated cluster seeds!")
        
        # Display results
        lst = self.__read_fastaptamer(join(seeds_dir,
                                           out_pref + '_seeds.fasta'),
                                      'cluster', int(self._seeds_num.value))
        new_lst = list()
        for i in lst:
            temp = [i[0]]
            temp.extend(map(float, i[1]))
            new_lst.append(temp)
        win = ResultsWindow(new_lst, ['Sequence', 'Rank', 'Count', 'RPM',
                                      'Cluster Rank'])
        win.show()

    def __runResults(self):
        """Compile results from multiple analyses."""
        import numpy as np
        import pandas as pd
        global win

        self.__addOutput("Compiling results ...")
        num = int(self._results_num.value)

        # Round 5
        self._results_counts.value = '/home/kevin/Documents/Code/comparison/htfr/seeds/round5_seeds.fasta'
        self._results_fastapt.value = '/home/kevin/Documents/Code/comparison/htfr/fastaptamer/round0_trim_round4_trim_round5_trim_enrich.tsv'
        self._results_aptani.value = '/home/kevin/Documents/Code/comparison/htfr/aptani/5rounds/seeds_Hairpins_data.csv'
        self._results_a_seeds.value = '/home/kevin/Documents/Code/comparison/htfr/seeds/round5_seeds_full.fastq'
        self._results_mpbind.value = '/home/kevin/Documents/Code/comparison/htfr/mpbind/MPBind_Out6/search_results.txt'
        self._results_aptacl.value = '/home/kevin/Documents/Code/comparison/htfr/aptacluster/config_5rounds.cfg'
        self._results_rnamotif.value = '/home/kevin/Documents/Code/comparison/htfr/rnamotif/5rounds/results.tab'

        # Round 8
        #self._results_counts.value = '/home/kevin/Documents/Code/comparison/htfr/seeds/round8_seeds.fasta'
        #self._results_fastapt.value = '/home/kevin/Documents/Code/comparison/htfr/fastaptamer/round0_trim_round4_trim_round8_trim_enrich.tsv'
        #self._results_aptani.value = '/home/kevin/Documents/Code/comparison/htfr/aptani/8rounds/seeds_Hairpins_data.csv'
        #self._results_a_seeds.value = '/home/kevin/Documents/Code/comparison/htfr/seeds/round8_seeds_full.fastq'
        #self._results_mpbind.value = '/home/kevin/Documents/Code/comparison/htfr/mpbind/MPBind_Out_r8_k6/search_results.txt'
        #self._results_aptacl.value = '/home/kevin/Documents/Code/comparison/htfr/aptacluster/config_8rounds.cfg'
        #self._results_rnamotif.value = '/home/kevin/Documents/Code/comparison/htfr/rnamotif/8rounds/results.tab'

        # Check counts
        if self._results_counts.value:
            if self._results_c_seeds.value: # Count cluster seeds
                lst = self.__read_fastaptamer(self._results_counts.value,
                                              'cluster', num)
            else:
                lst = self.__read_fastaptamer(self._results_counts.value,
                                              'count', num)
            counts = list()
            for i in lst:
                temp = [i[0]]
                temp.extend(map(float, i[1]))
                counts.append(temp)
            if self._results_c_seeds.value:
                temp = map(lambda x: [x[0], x[4]], counts)
            else:
                temp = map(lambda x: x[0:2], counts)    # Combine sequence and count tuple
            ranked_counts = sorted(temp, key=operator.itemgetter(1))

        # FASTAptamer
        if self._results_fastapt.value:
            lst, header = self.__read_fastaptamer_enrich(
                self._results_fastapt.value, num)
            df = pd.DataFrame(lst, columns=header, dtype='float')
            if self._results_c_seeds.value:
                df = df[df['Rank in Cluster (z)'] == 1] # Select cluster seeds
            if self._results_f_box.value == 1:      # Sort Z/X
                df.sort('Enrichment (z/y)', ascending=False, inplace=True)
            elif self._results_f_box.value == 2:    # Sort Z/Y
                df.sort('Enrichment (z/x)', ascending=False, inplace=True)
            ranked_fastapt = list()
            for rnk, seq in enumerate(df['Sequence'][:num]):
                ranked_fastapt.append([seq, float(rnk + 1)])

        # APTANI
        if self._results_aptani.value:
            # Check whether a directory or file was selected
            if self.__get_extension(self._results_aptani.value):
                aptani_dir = dirname(self._results_aptani.value)
            else:
                aptani_dir = self._results_aptani.value
            if num >= 15:
                temp = self.__read_aptani(aptani_dir, self._results_a_seeds.value, 100, self._constant5.value, self._constant3.value)
            else:
                temp = self.__read_aptani(aptani_dir, self._results_a_seeds.value, 50, self._constant5.value, self._constant3.value)
            # Re-sort data
            temp.sort(['Frequency', 'Identity'], ascending=[0,0], inplace=True)
            # Clean up data because the strings have extra spaces. Also delete
            # constant regions.
            aptani = list(temp.iloc[:, 5])
            ranked_aptani = list()
            for i in range(len(aptani)):
                temp = aptani[i].strip().upper().replace('U', 'T')
                temp = temp.replace(self._constant5.value, '').replace(self._constant3.value, '').replace('TTACATGCGAGATGACCAC', '')
                ranked_aptani.append([temp, 1.0 * (i + 1)])
            if len(ranked_aptani) > num:
                ranked_aptani = ranked_aptani[:num]

        # MPBind
        if self._results_mpbind.value:
            ranked_mpbind = self.__read_mpbind(self._results_mpbind.value, num)

        # AptaCluster
        if self._results_aptacl.value:
            d = self.__parse_config(self._results_aptacl.value)
            db = MySQLdb.connect(host="localhost",
                                 user=d['mysql_username'],
                                 passwd=d['mysql_password'],
                                 db=d['mysql_db_name'])
            c = db.cursor()
            table_name = d['mysql_table_prefix']
            rounds = d['cycle_ids']     # Get last cycle_ids
            # Get most abundant clusters from final round
            c.execute("SELECT * FROM {0} ORDER BY size DESC LIMIT {1};".format(
                table_name + '_cluster_meta_' + rounds, num))
            clusters = c.fetchall()
            ranked_aptacluster = list()
            for i in range(len(clusters)):
                tup = clusters[i]
                cid = tup[-1]
                c.execute("SELECT * FROM {0} where id={1};".format(
                    table_name + '_sequences_' + rounds, cid))
                nuc = c.fetchone()
                ranked_aptacluster.append([nuc[1], 1.0 * (i + 1)])

        # RNAmotifAnalysis
        # Call __read_rnamotif to read output from a tab-delimited file.
        if self._results_rnamotif.value:
            #ranked_ids = self.__read_rnamotif(self._results_rnamotif.value,
            #                                  self._results_seeds.value, num)
            ranked_rnamotif = self.__read_rnamotif(self._results_rnamotif.value, '', num)

        # Use Pandas to merge lists
        df_all = list()
        headers = ['Cluster Seed']
        total = 0
        if self._results_counts.value:
            df_c = pd.DataFrame(ranked_counts, columns=['Sequences', 'Frequency'])
            df_all.append(df_c)
            headers.append('Frequency')
            total += 1
        if self._results_aptani.value:
            df_apt = pd.DataFrame(ranked_aptani, columns=['Sequences', 'APTANI'])
            df_all.append(df_apt)
            headers.append('APTANI')
            total += 1
        if self._results_fastapt.value:
            df_f= pd.DataFrame(ranked_fastapt, columns=['Sequences', 'FASTAptamer'])
            df_all.append(df_f)
            headers.append('FASTAptamer')
            total += 1
        if self._results_mpbind.value:
            df_m = pd.DataFrame(ranked_mpbind, columns=['Sequences', 'MPBind'])
            df_all.append(df_m)
            headers.append('MPBind')
            total += 1
        if self._results_aptacl.value:
            df_ac = pd.DataFrame(ranked_aptacluster, columns=['Sequences', 'AptaCluster'])
            df_all.append(df_ac)
            headers.append('AptaCluster')
            total += 1
        if self._results_rnamotif.value:
            df_r = pd.DataFrame(ranked_rnamotif, columns=['Sequences', 'RNAmotifAnalysis'])
            df_all.append(df_r)
            headers.append('RNAmotifAnalysis')
            total += 1
        df = reduce(lambda left, right: pd.merge(left, right, on='Sequences', how='outer'), df_all)

        # Count number of programs that rank a sequence (e.g., 4 out of 6
        # selected this sequence). Also calculate the mean ranks, and replace
        # NaNs with a numeric value.
        appear = total - df.isnull().sum(axis=1)    # Count NaNs before replacing
        df = df.fillna(1.0 * (num+1))
        weights = df.mean(axis=1)
        df.loc[:, 'Ranks'] = weights
        headers.append('AptCompare Rank')
        df.loc[:, 'Appearances'] = appear
        headers.append('Appearances')

        # Convert back to a list of lists and cast numbers as floats, then
        # display in the results window. Set upper bound of ranks as the number
        # of motifs plus 1.
        lol = list()
        for i in range(len(df)):
            lol.append(list(df.loc[i]))
        ranks = list()
        for i in lol:
            temp = [i[0]]
            temp.extend([1.5*num if np.isnan(x) else float(x) for x in i[1:]])
            ranks.append(temp)

        self.__addOutput("Done!")
        win = ResultsWindow(ranks, headers)
        win.show()
    
    def __subdir(self, path, subdir):
        """Changes the path of a file so that it points to a subdirectory, subdir."""
        dir_name = dirname(path)
        base_name = basename(path)
        return join(dir_name, subdir, base_name)

    def __addOutput(self, txt):
        """Adds text to the plainTextEdit object."""
        self._output_log.__add__(txt)

    def __split_stdout(self, txt):
        """Splits multiline stdout and display in output log."""
        lst = txt.split("\n")
        for i in lst:
            self.__addOutput(i)

    def __get_extension(self, s):
        """Returns the extension of a file name."""
        if len(s) < 4:  # Too short to have an extension
            return None
        elif '.' not in s:
            return None
        else:
            idx = s.rfind('.')
            return s[idx+1:]

    def __runExit(self):
        """Exits the program."""  
        sys.exit()

    def __displayHelp(self):
        """Displays the Help window."""
        help_win = HelpWindow(self)
        help_win.show()

    def __displayAbout(self):
        """Displays the About window."""
        about_win = AboutWindow(self)
        about_win.show()

    def __read_fastaptamer(self, infile, informat, counter=1000):
        """Reads a FASTAptamer file. The input format must be 'count' or
        'cluster'."""
        d = dict()
        ih = open(infile, 'r')
        while counter > 0:
            line = ih.readline().strip()
            if not line:
                break
            else:
                seqid = line[1:]    
                seq = ih.readline().strip()
                if informat == 'count':
                    rank, reads, rpm = seqid.split('-')
                    d[seq] = ((rank, reads, rpm))
                else:
                    rank, reads, rpm, cluster, c_rank, dist = seqid.split('-')
                    d[seq] = ((rank, reads, rpm, cluster))
                counter -= 1
        sorted_d = sorted(d.items(), key=lambda x: x[1])
        ih.close()
        return sorted_d

    def __read_fastaptamer_enrich(self, infile, counter=1000):
        """Reads a tab-delimited file created by fastaptamer_enrich and
        returns a list of lists, one for each row."""
        lst = list()
        ih = open(infile, 'r')
        header = ih.readline().strip().split("\t")
        num_col = len(header)
        while counter > 0:
            line = ih.readline().strip()
            if not line:
                break
            else:
                temp = line.split('\t')
                for i in range(num_col-len(temp)):
                    temp.append('')
                row = [0 if x == '' else x for x in temp]
                lst.append(row)
        ih.close()
        return lst, header

    def __read_aptacluster(self, config_file):
        """Reads the AptaCluster output from a MySQL database."""
        d = self.__parse_config(config_file)
        db = MySQLdb.connect(host="localhost",
                             user=d['mysql_username'],
                             passwd=d['mysql_password'],
                             db=d['mysql_db_name'])
        c = db.cursor()
        table_name = d['mysql_table_prefix']
        rounds = d['cycle_ids']     # Last cycle_id in file
        # Get most abundant clusters from final round.
        c.execute("SELECT * FROM {0} ORDER BY size DESC LIMIT 10;".format(
            table_name + '_cluster_meta_' + rounds))
        clusters = c.fetchall()
        ranked_aptacluster = list()
        for i in range(len(clusters)):
            tup = clusters[i]
            cid = tup[-1]
            c.execute("SELECT * FROM {0} where id={1};".format(
                table_name + '_sequences_' + rounds, cid))
            nuc = c.fetchone()
            ranked_aptacluster.append([nuc[1], 1.0 * (i + 1)])        
        return ranked_aptacluster

    def __read_aptani(self, dirpath, seeds_file, top_motifs=3, c5='', c3=''):
        """Reads the APTANI output files. APTANI generates four files:
        Hairpins_data.csv, Intra_Strand_data.csv, Left_bulges_data.csv, and 
        Right_bulges_data.csv."""
        threshold_hp = 0.0#0.5
        threshold_is = 0.0#1.0
        threshold_lb = 0.0#2.0
        threshold_rb = 0.0#2.0

        # Read seeds file, which should be in FASTA format.
        seeds_list = list()
        ext = self.__get_extension(seeds_file)
        with open(seeds_file, 'r') as f:
            while True:
                line = f.readline().strip()
                if not line:
                    break
                else:
                    seq = f.readline().strip().replace('T', 'U')
                    seeds_list.append(seq)
                    if ext.lower() in ['fasta', 'fa']:
                        continue
                    elif ext.lower() in ['fastq', 'fq']:
                        f.readline()
                        f.readline()
                    else:
                        self.__addOutput("ERROR! Invalid cluster seed file!")
                
        # Hairpins
        hp_fn = join(dirpath, 'Hairpins_data.csv')
        with open(hp_fn, 'rb') as csvfile:
            hp_fh = csv.reader(csvfile, delimiter='\t')
            temp = list()
            next(hp_fh)     # Skip CSV header row
            for row in hp_fh:
                if float(row[3]) > threshold_hp:
                    seq = row[4].strip()
                    corr_seq = seq + c3[-2:].replace('T', 'U')  # Deal with bug in APTANI that cuts off 2 nt from the sequence
                    corr_dna = corr_seq.replace('U', 'T').replace(c5, '').replace(c3, '')
                    if corr_seq in seeds_list:
                        new_row = row[:4] + [corr_seq] + row[5:]
                        temp.append(tuple(new_row))
            # Define data object
            data = np.zeros((len(temp),), dtype=[('Consensus', 'object'),
                                                 ('Identity', 'f6'), ('Retrieved', 'object'),
                                                 ('Pop Left', 'f6'), ('Sequence', 'object'),
                                                 ('Frequency', 'f6'), ('Cluster', 'i4')])
            data[:] = temp
            hairpins = pd.DataFrame(data)
            hairpins.sort('Identity', ascending=False, inplace=True)

        # Intra-strand loops
        is_fn = join(dirpath, 'Intra_Strand_data.csv')
        with open(is_fn, 'rb') as csvfile:
            is_fh = csv.reader(csvfile, delimiter='\t')
            temp = list()
            next(is_fh)
            for row in is_fh:
                if (float(row[3]) > threshold_is) and (float(row[4]) > threshold_is):
                    seq = row[5].strip()
                    if seq in seeds_list:
                        temp.append(tuple(row))
            data = np.zeros((len(temp),), dtype=[('Consensus', 'object'),
                                                 ('Identity', 'f6'), ('Retrieved', 'object'),
                                                 ('Pop Left', 'f6'), ('Pop Right', 'f6'),
                                                 ('Sequence', 'object'), ('Frequency', 'f6'),
                                                 ('Cluster', 'i4')])
            data[:] = temp
            intra_strand = pd.DataFrame(data)
            intra_strand.sort('Identity', ascending=False, inplace=True)

        # Left bulges
        lb_fn = join(dirpath, 'Left_Bulges_data.csv')
        with open(lb_fn, 'rb') as csvfile:
            lb_fh = csv.reader(csvfile, delimiter='\t')
            temp = list()
            next(lb_fh)
            for row in lb_fh:
                if (float(row[3]) > threshold_lb) and (float(row[4]) > threshold_is):
                    seq = row[5].strip()
                    if seq in seeds_list:
                        temp.append(tuple(row))
            data = np.zeros((len(temp),), dtype=[('Consensus', 'object'),
                                                 ('Identity', 'f6'), ('Retrieved', 'object'),
                                                 ('Pop Left', 'f6'), ('Pop Right', 'f6'),
                                                 ('Sequence', 'object'), ('Frequency', 'f6'),
                                                 ('Cluster', 'i4')])
            data[:] = temp
            left_bulges = pd.DataFrame(data)
            left_bulges.sort('Identity', ascending=False, inplace=True)

        # Right bulges
        rb_fn = join(dirpath, 'Right_Bulges_data.csv')
        with open(rb_fn, 'rb') as csvfile:
            rb_fh = csv.reader(csvfile, delimiter='\t')
            temp = list()
            rb_fh.next()
            for row in rb_fh:
                if (float(row[3]) > threshold_is) and (float(row[4]) > threshold_rb):
                    seq = row[5].strip()
                    if seq in seeds_list:
                        temp.append(tuple(row))
            data = np.zeros((len(temp),), dtype=[('Consensus', 'object'),
                                                 ('Identity', 'f6'), ('Retrieved', 'object'),
                                                 ('Pop Left', 'f6'), ('Pop Right', 'f6'),
                                                 ('Sequence', 'object'), ('Frequency', 'f6'),
                                                 ('Cluster', 'i4')])
            data[:] = temp
            right_bulges = pd.DataFrame(data)
            right_bulges.sort('Identity', ascending=False, inplace=True)

        # Duplicate population column to hairpins because it only has 1
        # population column. Then select top motifs and rank them according to
        # 'Aptamer Frequency' and 'Identity' columns.
        hairpins.insert(4, 'Pop Right', hairpins.iloc[:,3])
        #motifs = pd.concat([hairpins.iloc[0:top_motifs], intra_strand.iloc[0:top_motifs],
        #                    left_bulges.iloc[0:top_motifs], right_bulges.iloc[0:top_motifs]])
        # Select rows with irow(slice(start, end)) because pandas 0.14+ will
        # give an IndexError if selecting rows outside of dimensions.
        motifs = pd.concat([hairpins.irow(slice(0, top_motifs)),
                            intra_strand.irow(slice(0, top_motifs)),
                            left_bulges.irow(slice(0, top_motifs)),
                            right_bulges.irow(slice(0, top_motifs))])

        # Remove constant regions and convert sequences to DNA. We have to do
        # this because of a bug in APTANI when it deals with hairpins. Then
        # we can sort it.
        for i in range(len(motifs)):
            seq = motifs.iloc[i, 5]
            seq = seq.strip().upper().replace('U', 'T')
            seq = seq.replace(self._constant5.value, '').replace(self._constant3.value, '').replace(self._constant3.value[:-2], '')
            motifs.iloc[i, 5] = seq
        motifs.sort(['Frequency', 'Identity'], ascending=[0,0], inplace=True)
        # Return the unique sequences
        u, indices = np.unique(motifs.iloc[:, 5], return_index=True)

        return(motifs.iloc[indices])

    def __read_mpbind(self, infile, num=10):
        """Reads an MPBind output file. This function assumes the output has
        been sorted, so it takes the first num lines."""
        mpbind = list()
        with open(infile, 'rb') as csvfile:
            fh = csv.reader(csvfile, delimiter='\t')
            fh.next()    # Ignore header
            temp = list()
            while num > 0:
                temp.append(next(fh))
                num -= 1
        for i in range(len(temp)):
            mpbind.append([temp[i][0], 1.0*(i+1)])
        return mpbind

    def __read_rnamotif(self, tab_file, seeds, num):
        """Reads and ranks the output from RNAmotifAnalysis. The output of
        subprocess.Popen() is a string with extra spaces around the tab
        characters. Thus, we split on the spaces, join them as one string, and
        then split again on newlines."""
        d = dict()

        if False:
            # Read FASTA file of cluster seeds so we know their sequences
            seq_dict = dict()
            with open(seeds, 'r') as f:
                while True:
                    line = f.readline().strip()
                    if not line:
                        break
                    else:
                        seqid = line[1:]
                        seq = f.readline().strip()
                        seq_dict[seqid] = seq

            # Read data
            cmd = "awk '$1!~/^#/{{print $2, \"\t\", $8}}' {0}".format(tab_file)
            p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)        
            stdout, stderr = p.communicate()
            temp = ''.join(stdout.split(' ')).split('\n')   # Remove spaces, split
            reader = csv.reader(temp[:-1], delimiter='\t')  # Last one is empty
            for row in reader:
                d[row[0]] = float(row[1])
            temp = sorted(d.items(), key=operator.itemgetter(1), reverse=False)
            temp = temp[:num]

            ranked_rnamotif = list()
            for i in range(len(temp)):
                seqid = temp[i]
                ranked_rnamotif.append((seq_dict[seq], 1.0 * (i + 1)))
            print ranked_rnamotif

        #ranked_rnamotif = [('TCAAACATCTCACAGATCAATCCAAGGGACCTCGTTAAAGGACGACTCCC', 1.0),
        #                   ('ACATAGTCGTCAACATGCGAACATTACATCGACTTCTTAGCATTTCGGTA', 2.0),
        #                   ('TAGGTTAGCGGCAGATCACTACAAAGCCCCTAGAGCACATGCTCCACCGCT', 3.0),
        #                   ('TGGAATAAGACCGCATGCGTCAATACATGCCCTTGATCCTCATTGTGCAT', 4.0),
        #                   ('GCGCGGGGATCATTTCGTGCGCCAGTATGTTTGCACACACCCGCGGTAGG', 5.0),
        #                   ('AAAGCTAATGCGATCATTTCATCGTTTCAACCTGTGTTACCGACCCACTG', 6.0),
        #                   ('CATTACGGCTACCCCGTGTAACGTCTAGCCAAAGTAGTACTAAAAGTCAG', 7.0),
        #                   ('GAAAGTAAGTACGATAGCAGCGCGTGGCGGGGAGATCAGTCCAATTCCTC', 8.0)]
        ranked_rnamotif = list()
        ctr = 1.0
        with open(tab_file, 'r') as f:
            while True:
                line = f.readline().strip()
                if not line:
                    break
                else:
                    ranked_rnamotif.append((line, ctr))
                    ctr += 1

        return ranked_rnamotif

    def __parse_config(self, infile):
        """Simple function to parse the AptaCluster configuration file."""
        d = dict()
        with open(infile, 'r') as f:
            lines = f.readlines()
            lines = [x for x in lines if ((x[0] != '\n') and (x[0] != '#'))]
            for line in lines:
                temp = map(lambda x: x.strip(), line.strip().split('='))
                d[temp[0]] = temp[1]
        return d
    
    def __set_progress(self, value=0):
        """Sets value and label of the ControlProgress widget."""
        if value == 0:
            # Reset to idle
            self._progress.label = 'Idle'
            self._progress.value = value
        else:
            if self._progress.label == 'Idle':
                # If not running, then change label to 'Running' and set at 1%
                self._progress.label = 'Running'
            self._progress.value = value

    def __dummyEvent(self):
        pass

class ResultsWindow(QtGui.QWidget):
    """Window for displaying results. A menubar is added at the top to allow
    for exiting and for copying all results to a spreadsheet.

    Adapted from https://www.daniweb.com/programming/software-development/code/447834/applying-pysides-qabstracttablemodel
    http://zetcode.com/gui/pyqt4/menusandtoolbars/"""
    def __init__(self, data_list, header, *args):
        """The window uses a QTableView object with a custom data model."""
        QtGui.QWidget.__init__(self, *args)
        self.setGeometry(200, 100, 1000, 800)
        self.setWindowTitle("Results")
        self.setWindowIcon(QtGui.QIcon('dna-chromosome.png'))
        table_model = ResultsModel(self, data_list, header)
        table_view = QtGui.QTableView()
        table_view.setModel(table_model)
        self.table_view = table_view
        self.table_model = table_model

        # Set font, then set column width to fit contents
        font = QtGui.QFont("Courier New", 10)
        table_view.setFont(font)
        table_view.resizeColumnsToContents()
        # Enable sorting
        table_view.setSortingEnabled(True)

        # Set up UI with a menu bar. The menu bar contains two menus, which
        # quit the application and copy all results.
        menubar = QtGui.QMenuBar(self)
        exit_action = QtGui.QAction('Quit', self)     # Exit application action
        exit_action.setShortcut('Ctrl+Q')
        exit_action.setStatusTip('Exit Application')
        exit_action.triggered.connect(QtGui.qApp.quit)
        copy_action = QtGui.QAction('Copy Results to Clipboard',
                                    self)   # Copy all results action
        copy_action.setShortcut('Ctrl+C')
        copy_action.triggered.connect(self.copy_as_string)
        file_menu = menubar.addMenu('&File')
        file_menu.addAction(exit_action)
        edit_menu = menubar.addMenu('&Edit')
        edit_menu.addAction(copy_action)
        # Add widgets to layout
        layout = QtGui.QVBoxLayout(self)
        layout.addWidget(menubar)
        layout.addWidget(table_view)
        self.setLayout(layout)

    def copy_as_string(self):
        """Copies the contents of the table as a string, which can be pasted
        in a spreadsheet program, such as Calc or Excel. We store values of a
        row in a list, and then call 'join' to insert tab characters between
        column values. We separate rows with a newline character."""
        self.clip = QtGui.QApplication.clipboard()
        table_model = self.table_model
        num_rows = len(table_model.mylist)
        num_columns = len(table_model.mylist[0])
        # Include columns headers
        s = "\t".join(table_model.header) + "\n"
        # Add data to string
        for row in range(num_rows):
            row_data = list()
            for column in range(num_columns):
                idx = table_model.index(row, column)
                row_data.append(str(table_model.data(idx,
                                                     QtCore.Qt.DisplayRole)))
            s += "\t".join(row_data) + "\n"
        self.clip.setText(s)

class ResultsModel(QtCore.QAbstractTableModel):
    """Model for the results."""
    def __init__(self, parent, mylist, header, *args):
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.mylist = mylist
        self.header = header

    def rowCount(self, parent):
        return len(self.mylist)

    def columnCount(self, parent):
        return len(self.mylist[0])

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role != QtCore.Qt.DisplayRole:
            return None
        return self.mylist[index.row()][index.column()]

    def headerData(self, col, orientation, role):
        if (orientation == QtCore.Qt.Horizontal and
            role == QtCore.Qt.DisplayRole):
            return self.header[col]
        return None

    def sort(self, col, order):
        # Sort table by given column number, col
        self.emit(QtCore.SIGNAL("layoutAboutToBeChanged()"))
        self.mylist = sorted(self.mylist,
                             key=operator.itemgetter(col))
        if order == QtCore.Qt.DescendingOrder:
            self.mylist.reverse()
        self.emit(QtCore.SIGNAL("layoutChanged()"))

class HelpWindow(QtGui.QMainWindow, aptcompare_help.Ui_HelpWindow):
    """Window to display Help window."""
    
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)  

class AboutWindow(QtGui.QMainWindow, aptcompare_about.Ui_AboutWindow):
    """Window to display About information."""
    
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.creditsButton.clicked.connect(self.credits_slot)
    
    def credits_slot(self):
        win = CreditsWindow(self)
        win.show()

class CreditsWindow(QtGui.QDialog):
    """Window to display Credits information."""
    
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle("Credits")
        self.setObjectName("Credits")
        self.resize(400, 300)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("dna-chromosome.png"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        self.textLabel = QtGui.QLabel(self)
        self.textLabel.setGeometry(QtCore.QRect(0, 0, 400, 300))
        self.textLabel.setWordWrap(True)
        self.textLabel.setText(("<html><body><font size=\"4\">The user "
            "interface was made Python, PyQt4, PyForms, and Qt 4 Designer."
            "<br/><br/>Icon made by Freepik from www.flaticon.com</font>"
            "</body></html>"))
        self.closeButton = QtGui.QPushButton(self)
        self.closeButton.setGeometry(QtCore.QRect(160, 250, 80, 25))
        self.closeButton.setObjectName("closeButton")
        self.closeButton.setText("Close")
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL("clicked()"),
                               self.close)        
        

##################################################################################################################
##################################################################################################################
##################################################################################################################

# Execute the application
if __name__ == "__main__":
    pyforms.startApp(Comparison)
    # Call the StandAloneContainer directly to set an app icon.
    #app = QtGui.QApplication(sys.argv)
    
    #w = StandAloneContainer(Comparison)
    #w.setGeometry(200, 200, 400, 800)
    #w.setWindowIcon(QIcon('icons/dna-chromosome.png'))
    #w.show()
    #app.exec_()
    
    #win = HelpWin()
    #win.show()
    #app.exec_()
