#!/usr/bin/env python

# Run RNAmotifAnalysis (covariance models) from AptCompare
#
# Note: Ensure that the following are in your PATH:
#   1. RNAmotifAnalysis (Bio::App::SELEX::RNAmotifAnalysis)
#   2. RNAalifold
#   3. mafft
#   4. Infernal 1.0.2
#   5. BASH or another shell
#
# Because RNAmotifAnalysis uses shell scripts, it will only work on Unix and
# Unix-like operating systems.

import sys
import argparse
import subprocess
import os

distance = 5
ncpu = 1
rounds = 10

def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description="Run RNAmotifAnalysis from AptCompare")
    parser.add_argument('-i', '--infile', required=True, help="Input file")
    parser.add_argument('-o', '--outfile', required=True, help="Output file")
    parser.add_argument('-s', '--seeds', required=True, help="File of cluster seeds")
    parser.add_argument('--c5', required=True, help="5' constant region")
    parser.add_argument('--c3', required=True, help="3' constant region")
    args = parser.parse_args()
    infile = args.infile
    outfile = args.outfile
    seeds = args.seeds
    constant5 = args.c5
    constant3 = args.c3
    
    # Check if file is a FASTQ file or sequence file (by checking file name).
    # RNAmotifAnalysis does not support FASTA files. Then execute 
    ext = infile[infile.rfind('.')+1:]
    if ext.lower() in ['fastq', 'fq']:
        cmd = ("RNAmotifAnalysis --fastq={0} --max_distance={1} --cpus={2} "
               "--run").format(infile, distance, ncpu)
    elif ext.lower() in ['sequence', 'seq']:
        cmd = ("RNAMotifAnalysis --simple={0} --max_distance{1} --cpus={2} "
               "--run").format(infile, distance, ncpu)
    else:
        raise ValueError("Error! File must be FASTQ or sequence")
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                         shell=True)
    stdout, stderr = p.communicate()
    print(stdout)
    
    # Run selex_covarianceSearch to calibrate and refine CMs. We use two loops,
    # first to create the scripts and then to run them.
    orig_dir = os.getcwd()      # Save current directory
    subprocess.call("cd batch_1.dir", shell=True)   # Change into directory
    print("Running selex_covarianceSearch")
    # Create scripts
    for i in range(1, rounds+1):
        cmd = ("selex_covarianceSearch --cm cluster_{0}_top.cm "
               "--fasta cluster_{1}_top.fasta "
               "--sto cluster_{2}_top.sto "
               "--rounds {3} --config ../cluster.cfg > "
               "cluster_{4}_top.sh").format(i, i, i, rounds, i)
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, shell=True)
        stdout, stderr = p.communicate()
        print(stdout)
    # Run scripts
    for i in range(1, rounds+1):
        cmd = "bash cluster_{0}_top.sh".format(i)
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, shell=True)
        stdout, stderr = p.communicate()
        print(stdout)
    
    # Search reduced sequence data set with CMs, using selex_seed_search.sh
    subprocess.call("mkdir seeds;cd seeds", shell=True)
    print("Searching against reduced sequence data set...")
    # Create scripts
    for i in range(1, rounds+1):
        cmd = ("selex_seed_search.sh {0} ../cluster_{1}_top_rnd{2}_aln.cm "
               "{3} {4} > cluster_{5}_search.sh").format(i, i, rounds-1, seeds,
               rounds, i)
        p = subprocess.call(cmd, shell=True)
        stdout, stderr = p.communicate()
        print(stdout)
    # Run scripts
    for i in range(1, rounds+1):
        cmd = "bash cluster_{0}_search.sh".format(i)
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, shell=True)
        stdout, stderr = p.communicate()
        print(stdout)
    
    # Determine remaining clusters that did not match, and seed new CM searches
    print("Identifying remaining clusters and seeding new search...")
    subprocess.call("mkdir remaining", shell=True)
    subprocess.call("awk '{if (NR % 2 == 1) print}' {0} > seeds_ids.txt".format(
                    seeds), shell=True)
    subprocess.call(("cat cluster_*_search_rnd{0}.fasta > "
                     "search_all_found.fasta").format(rounds+1), shell=True)
    subprocess.call(("awk '{if (NR % 2 == 1) print}' search_all_found.fasta > "
                     "search_all_found.txt"), shell=True)
    subprocess.call("sort -u search_all_found.txt > search_all_found_ids.txt",
                    shell=True)
    subprocess.call(("grep -v -f search_all_found_ids.txt seeds_ids.txt > "
                     "remaining_clusters.txt"), shell=True)
    subprocess.call(("grep -A1 --no-group-separator  -f remaining_clusters.txt "
                     "{0} > remaining/remaining_clusters.fasta").format(
                     seeds), shell=True)
    subprocess.call("awk '{if (NR % 2 == 1)}")
    subprocess.call("cd remaining", shell=True)
    
    
    # Align reduced data set to respective CMs, then visualize sequence and
    # structure conservation with R2R
    print("Aligning reduced data set to respective CMs...")
    for i in range(1, rounds+1):
        cmd = ("selex_seed_align.sh {0} cluster_{1}_search_rnd{2}.cm {3} > "
               "cluster_{4}_align.sh").format(i, i, rounds+1, seeds, i)
        p = subprocess.call(cmd, shell=True)
        stdout, stderr = p.communicate()
        print(stdout)
    for i in range(1, rounds+1):
        cmd = "bash cluster_{0}_align.sh".format(i)
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, shell=True)
        stdout, stderr = p.communicate()
        print(stdout)
    
    print("Completed first round of CM searching! Run R2R to generate new CMs")
    
    # Manual steps
    # Identify conserved base-pairs using R2R
    # Example R2R command: r2r --GSC-weighted-consensus <input-sto> <output-sto> 3 0.95 0.9 0.8 4 0.95 0.9 0.85 0.8 0.1
    
    # Add constant regions back to reduced data set and run final search
    seeds_const = seeds[:seeds.rfind('.')] + '_all.fasta'
    seeds_const = os.path.join(os.path.dirname(seeds), seeds_const)
    subprocess.call("selex_helper.py -i {0} -o {1} --add_constants "
                    "{2} {3}".format(seeds, seeds_const, constant5, constant3))
    
    # Run more rounds of CM searching and refinement
    # Search reduced data set (with constant regions)

if __name__ == "__main__":
    sys.exit(main())
