#!/usr/bin/env python

# Run MPBind from AptCompare

import sys
import argparse
import subprocess
from os.path import basename, dirname, join

def main(argv=None):
    if argv is None:
        argv = sys.argv
    
    parser = argparse.ArgumentParser(description="Run MPBind from AptCompare")
    parser.add_argument('--R0', required=True,
                        help="Initial round, 1 sequence per line")
    parser.add_argument('--RS', required=True,
                        help="SELEX rounds, given as a list")
    parser.add_argument('-RC', required=False, nargs='+',
                        help="Control rounds. Optional.")
    parser.add_argument('-o', '--outdir', required=False, default='MPBind_Out',
                        help="Output directory for MPBind")
    parser.add_argument('-k', '--kmer', required=True, type=int,
                        help="k-mer length")
    parser.add_argument('-u', '--unique', required=True, type=int,
                        help="Unique = 1, redundant = 2, both = 3")
    parser.add_argument('-s', '--search', required=True,
                        help="Search file for MPBind_Predict.py, 1 sequence per line")
    args = parser.parse_args()
    R0 = args.R0
    RS = args.RS
    #if args.RC:
    #    #RC = ','.join(args.RC)
    #    RC = args.RC
    #else:
    #    RC = None
    RC = args.RC if args.RC else None
    outdir = args.outdir
    k = args.kmer
    unique_reads = args.unique
    search_file = args.search
    
    if unique_reads == 1:
        train_file = "{0}/Unique_Reads.Combined_Z_Score.train.{1}mer".format(outdir, k)
    elif unique_reads == 2:
        train_file = "{0}/Redundant_Reads.Combined_Z_Score.train.{1}mer".format(outdir, k)
    elif unique_reads == 3:
        train_file = "{0}/Unique_Reads.Combined_Z_Score.train.{1}mer".format(outdir, k)
    else:
        print("Invalid value! The 'unique_reads' value must be 1, 2, or 3.")
        raise
    
    # Run MPBind
    if RC:
        RC_flag = "-R0 {0}".format(RC)
    else:
        RC_flag = ""
    # Train MPBind
    cmd = ("MPBind_Train.py -R0 {0} -RS {1} {2} -nmer {3} -U {4} "
           "-Out {5}").format(R0, RS, RC_flag, k, unique_reads, outdir)
    print("Running command: {0}".format(cmd))
    subprocess.call(cmd, shell=True)
    # Use Trained values to predict meta-Z-scores
    cmd = ("MPBind_Predict.py -Train {0} -Aptamer {1} -Sort {2} "
           "-Out {3}").format(train_file, search_file, True,
                              join(outdir, 'search_results.txt'))
    print("Running command: {0}".format(cmd))
    subprocess.call(cmd, shell=True)
    

if __name__ == "__main__":
    sys.exit(main())
