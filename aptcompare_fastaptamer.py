#!/usr/bin/env python

# Run FASTAptamer from AptCompare
# Script assumes that the output directory, 'fastaptamer', has been created.

import sys
import argparse
import subprocess
from os.path import basename, dirname, join

def main(argv=None):
    if argv is None:
        argv = sys.argv
    
    parser = argparse.ArgumentParser(description="Run FASTAptamer from AptCompare")
    parser.add_argument('-i', '--infile', required=True, nargs='+',
                        help="Input FASTQ files")
    parser.add_argument('-d', '--distance', required=False, default=5, type=int,
                        help="Edit distance for clustering")    
    parser.add_argument('-f', '--filter', required=False, default=5, type=int,
                        help="Read filter for clustering")
    parser.add_argument('-s', '--seeds', required=False, default=None,
                        help="Run fastaptamer_enrich with a cluster seed file")
    args = parser.parse_args()
    files = args.infile
    distance = args.distance
    filt = args.filter
    seeds_file = args.seeds
    num_files = len(files)
    filenames = map(basename, files)
    prefix_list = map(lambda s: s[:s.rfind('.')], filenames)
    count_files = list()
    cluster_files = list()
    
    # Run fastaptamer_count
    for i in range(num_files):
        infile = files[i]
        outfile = prefix_list[i] + "_count.fasta"
        outfile = join(dirname(infile), 'fastaptamer', outfile)
        count_files.append(outfile)
        cmd = "fastaptamer_count -i {0} -o {1}".format(infile, outfile)
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, shell=True)
        out, err = p.communicate()
        print(out)
    
    # Run fastaptamer_cluster
    for i in range(len(count_files)):
        infile = count_files[i]
        outfile = prefix_list[i] + "_cluster.fasta"
        outfile = join(dirname(infile), outfile)
        cluster_files.append(outfile)
        cmd = "fastaptamer_cluster -i {0} -o {1} -d {2} -f {3}".format(infile,
                outfile, distance, filt)
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, shell=True)
        out, err = p.communicate()
        print(out)
    
    # Run fastaptamer_enrich. If a seed file is given, then the third file
    # is the seed file. We use two count files and a cluster file as the third
    # one, so that the cluster files in the third round are more likely to be
    # present in the preceding rounds.
    if seeds_file:
        enrich_files = count_files[:-1] + [seeds_file]
    else:
        enrich_files = count_files[:-1] + cluster_files[len(cluster_files)-1:]
    if enrich_files:
        #infile = enrich_files[i:i+3]
        if len(enrich_files) == 4:
            infile = [enrich_files[0], enrich_files[2], enrich_files[3]]
            outfile = "_".join([prefix_list[0], prefix_list[2], prefix_list[3]]) + "_enrich.tsv"
            outfile = join(dirname(infile[0]), outfile)
            print(outfile)
        else:
            infile = enrich_files[0:3]
            outfile = "_".join(prefix_list) + "_enrich.tsv"
            outfile = join(dirname(infile[0]), outfile)
        cmd = "fastaptamer_enrich -x {0} -y {1} -z {2} -o {3} -f {4}".format(
            infile[0], infile[1], infile[2], outfile, filt)
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, shell=True)
        out, err = p.communicate()
        print(out)
    return
    

if __name__ == "__main__":
    sys.exit(main())
